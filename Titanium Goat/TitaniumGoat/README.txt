             / /
          (\/_//`)
           /   '/
          0  0   \
         /        \
        /    __/   \
       /,  _/ \     \_
       `-./ )  |     ~^~^~^~^~^~^~^~\~.
           (   /                     \_}
              |         Ti    /      |
              ;     |         \      /
               \/ ,/           \    |
               / /~~|~|~~~~~~|~|\   |
              / /   | |      | | `\ \
             / /    | |      | |   \ \
            / (     | |      | |    \ \
     jgs   /,_)    /__)     /__)   /,_/
     '''''"""""'''""""""'''""""""''"""""''"""""'
     |          PROJECT TITANIUM GOAT          |
     |          ''""""""'''""""""''""          |
     |            Chadd Ingersoll              |
     |            William Hearter              |
     |             Puneet Sethi                |
     |            Nathan Secrist               |
     -------------------------------------------

	 '''''"""""'''""""""'''""""""''"""""''"""""'
     |                                         |
     |        GENERAL GOAT INFORMATION         |
     |                                         |
     -------------------------------------------

	 Titanium Goat is a Windows game which uses the Microsoft Kinect for Windows sensor.
	 It is important to note that the game must be attached to a Visual Studio instance
	 at runtime in order to us an Xbox 360 Kinect sensor. In order to use the software
	 in a non-development environment, a "Kinect for Windows" sensor must be used. This
	 is due to licencing requirements from Microsoft and is inforced at the SDK level.

	 Application information
	 ------------------------

	 Language:			Managed C#
	 License:			Microsoft Permissive Licence (MS-PL)
	 Insertion point:	Program.Main()

	 '''''"""""'''""""""'''""""""''"""""''"""""'
     |                                         |
     |             BUILDING THE GOAT           |
     |                                         |
     -------------------------------------------

		Building this project rquires the following;

			-Visual Studio Express 2010 SP1(or higher)
				-Service pack 1 installer for 2010; http://www.microsoft.com/en-us/download/details.aspx?id=23691
				-SP1 patch for Team Foundation Service; http://go.microsoft.com/fwlink/?LinkID=212065
				-Visual Studio 2012 requires XNA 4.0 Extensions from VS2010; http://ryan-lange.com/xna-game-studio-4-0-visual-studio-2012/
			-XNA Game Studio 4.0 Refresh
				http://www.microsoft.com/en-us/download/details.aspx?id=27599
			-Kinect for Windows SDK v1.6 (1.7 has also ben tested to work, but is not supported)
				http://www.microsoft.com/en-us/download/details.aspx?id=34808

	 '''''"""""'''""""""'''""""""''"""""''"""""'
     |                                         |
     |              TESTING THE GOAT           |
     |                                         |
     -------------------------------------------

		This solution package contains a Visual Studio test suite called TitaniumGoatTests.
		You can run the tests that are contained in the test suite by following these steps;

			NOTE: Visual Studio Express does not support unit tests, you will need a licenced
					vopy of Visual Studio 2010 in order to run unit tests.

			In Visual Studio 2012 Professional or Ultimate
			------------------------------
			1. Open the TitaniumGoat.sln file in Visual Studio
			2. Select the Test menu
			3. Select the Debug option
			4. Select "All Tests in Solution"

			In Visual Studio 2010 Professional
			------------------------------
			1. Right click on the test project
			2. Select Debug
			3. Select "Run in new instance"

			The tests will run and you will be presented with the results in the output window
			at the bottom of your screen.

	 '''''"""""'''""""""'''""""""''"""""''"""""'
     |                                         |
     |        GETTING HELP WITH THE GOAT       |
     |                                         |
     -------------------------------------------

	 For support, please write to the Titanium Goat mailing list, at;

			PTG@ASU.edu
