using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;


namespace TitaniumGoat
{
    /// <summary>
    /// Game component that checks for updates to user inputs via Keyboard.
    /// </summary>
    public class KeyboardInputHandler : Microsoft.Xna.Framework.GameComponent
    {

        static KeyboardState keyboardState;
        static KeyboardState lastKeyboardState;

        public KeyboardInputHandler(Game game)
            : base(game)
        {
            keyboardState = Keyboard.GetState();
        }


        /// <summary>
        /// Returns the current state the keyboard is in.
        /// </summary>
        public static KeyboardState KeyboardState
        {
            get { return keyboardState; }
        }

        /// <summary>
        /// Returns the last state the keyboard was in.
        /// </summary>
        public static KeyboardState LastKeyboardState
        {
            get { return lastKeyboardState; }
        }


        /// <summary>
        /// Returns true if the key is being released.
        /// </summary>
        public static bool KeyReleased(Keys key)
        {
            return keyboardState.IsKeyUp(key) &&
                lastKeyboardState.IsKeyDown(key);
        }


        /// <summary>
        /// Returns true if the key is pressed.
        /// </summary>
        public static bool KeyPressed(Keys key)
        {
            return keyboardState.IsKeyDown(key) &&
                lastKeyboardState.IsKeyUp(key);
        }


        /// <summary>
        /// Returns true if the key is currently pressed down.
        /// </summary>
        public static bool KeyDown(Keys key)
        {
            return keyboardState.IsKeyDown(key);
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {

            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            lastKeyboardState = keyboardState;
            keyboardState = Keyboard.GetState();

            base.Update(gameTime);
        }
    }
}
