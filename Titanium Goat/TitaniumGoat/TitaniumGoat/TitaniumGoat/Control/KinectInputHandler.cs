﻿using Microsoft.Kinect;
using Microsoft.Xna.Framework;
using System;
using System.Timers;
using TitaniumGoatKinectLib;
using TitaniumGoatKinectLib.Segments;

namespace TitaniumGoat
{
    public class KinectInputHandler : IDisposable
    {
        public static int kinectMouseX, kinectMouseY;
        public static int shoulderCenterX, hipCenterX;
        public static bool skeletonTracked = false;
        public static String gesture = "";

        private Timer _clearTimer;

        private static Joint[] headJoints = new Joint[3];
        private static Joint[] shoulderJoints = new Joint[3];
        private static Joint[] hipJoints = new Joint[3];
        private static Joint[] lfootJoints = new Joint[3];

        // Track whether Dispose has been called. 
        private bool disposed = false;


        /// <summary>
        /// This control creates variables to hold the skeleton data and GestureController
        /// </summary>
        private String Gesture = "";
        private GestureController gestureController;
        private Skeleton[] skeletons = new Skeleton[0];

        private KinectChooser kinect = GameServices.GetService<KinectChooser>();

        /// <summary>
        /// Create new KinectInputHandler
        /// </summary>
        public KinectInputHandler()
        {
            // add timer for clearing last detected gesture
            _clearTimer = new Timer(2000);
            _clearTimer.Elapsed += new ElapsedEventHandler(clearTimer_Elapsed);

            // Initilize the gestureController
            gestureController = new GestureController();
            gestureController.GestureRecognized += OnGestureRecognized;

            if (kinect.Sensor != null)
            {
                kinect.Sensor.SkeletonFrameReady += OnSkeletonFrameReady;
            }
        }

        /// <summary>
        /// Tracks joints to see if a jump occured
        /// </summary>
        /// 
        public static bool checkIfJump()
        {
            bool jump = true;
            for (int i = 0; i < headJoints.Length - 1; i++)
            {
                //check to see if change height change occurs at several bones incase player is just ducking/etc...
                if ((headJoints[i + 1].Position.Y - headJoints[i].Position.Y) <= 0.002 || (shoulderJoints[i + 1].Position.Y - shoulderJoints[i].Position.Y) <= 0.002 ||
                    (hipJoints[i + 1].Position.Y - hipJoints[i].Position.Y) <= 0.002 || (lfootJoints[i + 1].Position.Y - lfootJoints[i].Position.Y) <= 0.002)
                {
                    jump = false;
                }
            }
            if (jump)
                DebugBuffer.write("Jump occured");
            return jump;
        }

        /// <summary>
        /// Helper function to register all available 
        /// </summary>
        public void RegisterGestures()
        {
            // gestures specific to Titanium Goat
            IRelativeGestureSegment[] selectBottomLeftSegments = new IRelativeGestureSegment[15];
            SelectBottomLeftSegment selectBottomLeftSegment = new SelectBottomLeftSegment();
            for (int i = 0; i < 15; i++)
            {
                selectBottomLeftSegments[i] = selectBottomLeftSegment;
            }
            gestureController.AddGesture("Select Bottom Left", selectBottomLeftSegments);
            IRelativeGestureSegment[] selectBottomRightSegments = new IRelativeGestureSegment[15];
            SelectBottomRightSegment selectBottomRightSegment = new SelectBottomRightSegment();
            for (int i = 0; i < 15; i++)
            {
                selectBottomRightSegments[i] = selectBottomRightSegment;
            }
            gestureController.AddGesture("Select Bottom Right", selectBottomRightSegments);
            IRelativeGestureSegment[] selectTopLeftSegments = new IRelativeGestureSegment[15];
            SelectTopLeftSegment selectTopLeftSegment = new SelectTopLeftSegment();
            for (int i = 0; i < 15; i++)
            {
                selectTopLeftSegments[i] = selectTopLeftSegment;
            }
            gestureController.AddGesture("Select Top Left", selectTopLeftSegments);
            IRelativeGestureSegment[] selectTopRightSegments = new IRelativeGestureSegment[15];
            SelectTopRightSegment selectTopRightSegment = new SelectTopRightSegment();
            for (int i = 0; i < 15; i++)
            {
                selectTopRightSegments[i] = selectTopRightSegment;
            }
            gestureController.AddGesture("Select Top Right", selectTopRightSegments);
            IRelativeGestureSegment[] selectDuckSegment = new IRelativeGestureSegment[5];
            DuckSegment1 selectDuckSegment1 = new DuckSegment1();
            for (int i = 0; i < 5; i++)
            {
                selectDuckSegment[i] = selectDuckSegment1;
            }
            gestureController.AddGesture("Select Duck", selectDuckSegment);
        }

        // Implement IDisposable. - http://msdn.microsoft.com/query/dev11.query?appId=Dev11IDEF1&l=EN-US&k=k(CA1001)&rd=true
        // Do not make this method virtual. 
        // A derived class should not be able to override this method. 
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method. 
            // Therefore, you should call GC.SupressFinalize to 
            // take this object off the finalization queue 
            // and prevent finalization code for this object 
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Executes everytime a gesture is recogized and sets KinectInputHandler.gesture appropriatly
        /// </summary>
        /// <param name="sender">sending onject (kinect event)</param>
        /// <param name="e">Arguments of GestureEvent</param>
        private void OnGestureRecognized(object sender, GestureEventArgs e)
        {
            switch (e.GestureName)
            {
                case "Select Bottom Left":
                    Gesture = "Select Bottom Left";
                    DebugBuffer.write("Select Bottom Left gesture recognized.");
                    break;
                case "Select Bottom Right":
                    Gesture = "Select Bottom Right";
                    DebugBuffer.write("Select Bottom Right gesture recognized.");
                    break;
                case "Select Top Left":
                    Gesture = "Select Top Left";
                    DebugBuffer.write("Select Top Left gesture recognized.");
                    break;
                case "Select Top Right":
                    Gesture = "Select Top Right";
                    DebugBuffer.write("Select Top Right gesture recognized.");
                    break;
                case "Select Duck":
                    Gesture = "Duck";
                    DebugBuffer.write("Ducking");
                    break;
                default:
                    Gesture = "Standing";
                    break;
            }
            _clearTimer.Start();
        }

        /// <summary>
        /// Pass the skeleton data to the gestureController.
        /// http://www.exceptontuesdays.com/gestures-with-microsoft-kinect-for-windows-sdk-v1-5/
        /// </summary>
        private void OnSkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            using (SkeletonFrame frame = e.OpenSkeletonFrame())
            {
                if (frame == null)
                    return;
                // resize the skeletons array if needed
                if (skeletons.Length != frame.SkeletonArrayLength)
                {
                    skeletons = new Skeleton[frame.SkeletonArrayLength];
                }
                // get the skeleton data
                frame.CopySkeletonDataTo(skeletons);
                KinectInputHandler.skeletonTracked = false;
                foreach (var skeleton in skeletons)
                {

                    // skip the skeleton if it is not being tracked
                    if (skeleton.TrackingState != SkeletonTrackingState.Tracked)
                    {
                        continue;
                    }
                    KinectInputHandler.skeletonTracked = true;
                    trackSkeletonOvertime(skeleton);
                    gestureController.UpdateAllGestures(skeleton);
                    gesture = Gesture;
                    // make sure both hands are tracked
                    if (skeleton.Joints[JointType.HandLeft].TrackingState == JointTrackingState.Tracked &&
                        skeleton.Joints[JointType.HandRight].TrackingState == JointTrackingState.Tracked)
                    {
                        int cursorX, cursorY, centerX;
                        // get the left and right hand Joints
                        Joint jointRight = skeleton.Joints[JointType.HandRight];
                        Joint jointLeft = skeleton.Joints[JointType.HandLeft];
                        Joint shoulderCenter = skeleton.Joints[JointType.ShoulderCenter];
                        Joint hipCenter = skeleton.Joints[JointType.HipCenter];
                        // scale those Joints to the primary screen width and height
                        Joint scaledRight = KinectInputScaling.ScaleToHand(jointRight, 1280, 720, skeleton.Joints[JointType.Head].Position.Y, skeleton.Joints[JointType.HipCenter].Position.Y);
                        Joint scaledLeft = KinectInputScaling.ScaleToHand(jointLeft, 1280, 720, skeleton.Joints[JointType.Head].Position.Y, skeleton.Joints[JointType.HipCenter].Position.Y);
                        Joint scaledShoulderCenter = KinectInputScaling.ScaleTo(shoulderCenter, 1280, 720);
                        Joint scaledHipCenter = KinectInputScaling.ScaleTo(hipCenter, 1280, 720);
                        cursorX = (int)scaledRight.Position.X;
                        cursorY = (int)scaledRight.Position.Y;
                        centerX = (int)scaledShoulderCenter.Position.X;
                        kinectMouseX = cursorX;
                        kinectMouseY = cursorY;
                        shoulderCenterX = centerX;
                        centerX = (int)scaledHipCenter.Position.X;
                        hipCenterX = centerX;
                    }
                }
            }
        }

        /// <summary>
        /// Clear the timer.
        /// http://www.exceptontuesdays.com/gestures-with-microsoft-kinect-for-windows-sdk-v1-5/
        /// </summary>
        private void clearTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Gesture = "";
            _clearTimer.Stop();
        }

        /// <summary>
        /// Tracks different joints over time for any dynamic movements like jumping
        /// </summary>
        private static void trackSkeletonOvertime(Skeleton skeleton)
        {
            for (int i = 0; i < headJoints.Length - 1; i++)
            {
                headJoints[i] = headJoints[i + 1];
                shoulderJoints[i] = shoulderJoints[i + 1];
                hipJoints[i] = hipJoints[i + 1];
                lfootJoints[i] = lfootJoints[i + 1];
            }
            headJoints[2] = skeleton.Joints[JointType.Head];
            shoulderJoints[2] = skeleton.Joints[JointType.ShoulderCenter];
            hipJoints[2] = skeleton.Joints[JointType.HipCenter];
            lfootJoints[2] = skeleton.Joints[JointType.FootLeft];
        }

        // Dispose(bool disposing) executes in two distinct scenarios. 
        // If disposing equals true, the method has been called directly 
        // or indirectly by a user's code. Managed and unmanaged resources 
        // can be disposed. 
        // If disposing equals false, the method has been called by the 
        // runtime from inside the finalizer and you should not reference 
        // other objects. Only unmanaged resources can be disposed. 
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called. 
            if(!this.disposed)
            {
                // If disposing equals true, dispose all managed 
                // and unmanaged resources. 
                if(disposing)
                {
                    //Dispose managed resources here
                    _clearTimer.Dispose();
                }

                // Call the appropriate methods to clean up 
                // unmanaged resources here. 
                // If disposing is false, 
                // only the following code is executed.
                //

                // Note disposing has been done.
                disposed = true;

            }
        }


        // Use interop to call the method necessary 
        // to clean up the unmanaged resource.
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1060:MovePInvokesToNativeMethodsClass"), System.Runtime.InteropServices.DllImport("Kernel32")]
        private extern static Boolean CloseHandle(IntPtr handle);

        // Use C# destructor syntax for finalization code. 
        // This destructor will run only if the Dispose method 
        // does not get called. 
        // It gives your base class the opportunity to finalize. 
        // Do not provide destructors in types derived from this class.
        ~KinectInputHandler()
        {
            // Do not re-create Dispose clean-up code here. 
            // Calling Dispose(false) is optimal in terms of 
            // readability and maintainability.
            Dispose(false);
        }

    }

    /// <summary>
    /// Handles join scaling from Kinect sensor. Used to better accomodate people of all sizes.
    /// Methods are required to be in a static class
    /// </summary>
    public static class KinectInputScaling
    {

        /// <summary>
        /// Scales a joint to the screen
        /// </summary>
        public static Joint ScaleTo(this Joint joint, int width, int height, float skeletonMaxX, float skeletonMaxY)
        {
            Microsoft.Kinect.SkeletonPoint pos = new SkeletonPoint()
            {
                X = Scale(width, skeletonMaxX, joint.Position.X),
                Y = Scale(height, skeletonMaxY, -joint.Position.Y),
                Z = joint.Position.Z
            };

            joint.Position = pos;
            return joint;
        }

        public static Joint ScaleTo(this Joint joint, int width, int height)
        {
            return ScaleTo(joint, width, height, 0.60f, 0.40f);
        }

        /// <summary>
        /// Scales hand to the scren based off position between head and hip for more accurate controls
        /// </summary>
        public static Joint ScaleToHand(this Joint joint, int width, int height, float heady, float hipy)
        {
            Joint handJoint = new Joint();
            float temp = MathHelper.Clamp(joint.Position.Y, hipy, heady);
            temp -= hipy;
            temp /= (heady - hipy);
            temp = MathHelper.Clamp(joint.Position.Y, 0f, 1f);
            temp *= 2;
            temp -= 1;
            var newPosition = new SkeletonPoint();
            newPosition.X = joint.Position.X;
            newPosition.Y = temp;
            handJoint.Position = newPosition;

            return ScaleTo(handJoint, width, height, 0.60f, 0.40f);
        }

        public static float Scale(int maxPixel, float maxSkeleton, float position)
        {
            float value = ((((maxPixel / maxSkeleton) / 2) * position) + (maxPixel / 2));
            if (value > maxPixel)
                return maxPixel;
            if (value < 0)
                return 0;
            return value;
        }
    }
}
