﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace TitaniumGoat
{

    /// <summary>
    /// Class used to create a left/right selection object to select a value from a list of items.
    /// </summary>
    class LeftRightSelection : ScreenObject
    {
        public event EventHandler SelectionChanged;

         string[] items = { "Low", "Medium", "High" }; //audiolevel
         //string[] lightLevels = { "Light", "Medium", "Dark" };



        Texture2D left;
        Texture2D right;
        Texture2D end;
        SpriteFont debugFont;

        Color selectedColor = Color.Red;
        int maxItemWidth=0;
        int selectedItem=0;



        /// <summary>
        /// Gets/Sets the color of the text when object has focus, default is red.
        /// </summary>
        public Color SelectedColor
        {
            get { return selectedColor; }
            set { selectedColor = value; }
        }

        /// <summary>
        /// Constructor that creates a blank left/right selection.
        /// </summary>
        public LeftRightSelection()
        {
            position = Vector2.Zero;
        }

        /// <summary>
        /// Constructor for left/right selection with textures.
        /// </summary>
        /// <param name="leftArrow">Texture2D leftArrow</param>
        /// <param name="rightArrow">Texture2D rightArrow</param>
        /// <param name="stop">Texture2D stop</param>
        public LeftRightSelection(Texture2D leftArrow, Texture2D rightArrow, Texture2D stop)
        {
            left = leftArrow;
            right = rightArrow;
            end = stop;
            position = Vector2.Zero;
        }

        /// <summary>
        /// Loads content for the left/right selection.
        /// </summary>
        /// <param name="theContentManager">ContentManager theContentManager</param>
        /// <param name="theAssetName">string theAssetName</param>
        public void LoadContent(ContentManager theContentManager, string theAssetName)
        {
            debugFont = theContentManager.Load<SpriteFont>(@"Fonts\\DebugFont");
        }

        /// <summary>
        /// Sets the list for the selection. Takes in a max width, for spacing the string with the 
        /// objects on the side.
        /// </summary>
        /// <param name="items">string[] items</param>
        /// <param name="maxWidth">int maxWidth</param>
        public void setList(string[] items, int maxWidth)
        {
            this.items = null;

            this.items = items;

            maxItemWidth = maxWidth;
        }

        public string[] getList()
        {
            return items;
        }

        public string getSelectedItem()
        {
            return items[selectedItem];
        }

        /// <summary>
        /// Updates the left/right selection, currently has no use.
        /// </summary>
        /// <param name="gameTime">GameTime gameTime</param>
        public override void Update(GameTime gameTime)
        {
        }

        /// <summary>
        /// Draws the lift/right selection.
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch spriteBatch</param>
        public override void Draw(SpriteBatch spriteBatch)
        {
            Vector2 drawTo = position;

            if (selectedItem != 0)
                spriteBatch.Draw(left, drawTo, Color.White);
            else
                spriteBatch.Draw(end, drawTo, Color.White);

            drawTo.X += left.Width;

            float itemWidth = debugFont.MeasureString(items[selectedItem]).X;
            float offset = (maxItemWidth - itemWidth) / 2;

            drawTo.X += offset;

            if (isFocus)
                spriteBatch.DrawString(debugFont, items[selectedItem], drawTo, selectedColor);
            else
                spriteBatch.DrawString(debugFont, items[selectedItem], drawTo, Color.White);

            drawTo.X += -1 * offset + maxItemWidth;

            if (selectedItem != items.Length - 1)
                spriteBatch.Draw(right, drawTo, Color.White);
            else
                spriteBatch.Draw(end, drawTo, Color.White);
        }

        /// <summary>
        /// Handles input for the left/right selection. 
        /// Currently left goes to prev item, right goes to next item.
        /// </summary>
        /// <param name="playerIndex">PlayerIndex playerIndex</param>
        public override void handleInput(PlayerIndex playerIndex)
        {
            if (items.Length == 0)
                return;


            if (KeyboardInputHandler.KeyPressed(Keys.Left))
            {
                selectedItem--;
                if (selectedItem < 0)
                    selectedItem = 0;
                OnSelectionChanged();
            }

            if (KeyboardInputHandler.KeyPressed(Keys.Right))
            {
                selectedItem++;
                if (selectedItem >= items.Length)
                    selectedItem = items.Length - 1;
                OnSelectionChanged();
            }
        }

        /// <summary>
        /// For when an event handler is assigned to the object, allows updating of outside methods.
        /// Example: Automatically changes the lighting, instead of having to accept ok.
        /// </summary>
        protected void OnSelectionChanged()
        {
            if (SelectionChanged != null)
            {
                SelectionChanged(this, null);
            }
        }
    }
}
