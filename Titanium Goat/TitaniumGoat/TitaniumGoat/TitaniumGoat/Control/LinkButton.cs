﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace TitaniumGoat
{

    /// <summary>
    /// Class used to define a button object with an event handler when an action is performed.
    class LinkButton : ScreenObject
    {
        SpriteFont debugFont;
        Texture2D button;
        

        public event EventHandler Selected;


        /// <summary>
        /// Constructor for link button, sets default position of the point to 0,0. 
        /// </summary>
        public LinkButton()
        {
            position = Vector2.Zero;
        }




        /// <summary>
        /// Loads content for the link button.
        /// </summary>
        /// <param name="theContentManager">ContentManager theContentManager</param>
        /// <param name="theAssetName">string theAssetName</param>
        public void LoadContent(ContentManager theContentManager, string theAssetName)
        {
            debugFont = theContentManager.Load<SpriteFont>(@"Fonts\\DebugFont");
            button = theContentManager.Load<Texture2D>(theAssetName);
        }

        /// <summary>
        /// Updates the link button, currently has no use.
        /// </summary>
        /// <param name="gameTime">GameTime gameTime</param>
        public override void Update(GameTime gameTime)
        {
            
        }




        /// <summary>
        /// Draws the link button.
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch spriteBatch</param>
        public override void Draw(SpriteBatch spriteBatch)
        {


            if (isFocus)
                spriteBatch.Draw(button, position, Color.Blue);
            else
                spriteBatch.Draw(button, position, Color.White);

        }


        /// <summary>
        /// Handles input for the linkbutton.
        /// Currently enter causes an event.
        /// </summary>
        /// <param name="playerIndex">PlayerIndex playerIndex</param>
        public override void handleInput(PlayerIndex playerIndex)
        {
            if (!isFocus)
                return;

            if (KeyboardInputHandler.KeyReleased(Keys.Enter)){
                OnSelected(null);
            }
                
        }


        protected void OnSelected(EventArgs e)
        {
            if (Selected != null)
            {
                Selected(this, e);
            }
        }


    }
}
