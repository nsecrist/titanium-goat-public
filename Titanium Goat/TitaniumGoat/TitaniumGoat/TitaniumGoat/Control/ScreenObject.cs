using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace TitaniumGoat
{
    /// <summary>
    /// An abstract class used to define interactable objects, such as buttons.
    /// </summary>
    public abstract class ScreenObject
    {
        protected Vector2 position;

        protected Vector2 size;

        public bool isFocus = false;

        public bool enabled = true;

        public bool Enabled
        {
            get { return enabled; }
            set { enabled = value; }
        }

        public bool IsFocus
        {
            get { return isFocus; }
            set { isFocus = value; }
        }



        /// <summary>
        /// Virtual method for loading content
        /// </summary>
        public virtual void LoadContent() { }


        /// <summary>
        /// Virtual method for unloading content
        /// </summary>
        public virtual void UnloadContent() { }

        /// <summary>
        /// Virtual method for updating the object
        /// </summary>
        /// <param name="gameTime">GameTime gameTime</param>
        public virtual void Update(GameTime gameTime) { }

        /// <summary>
        /// Virtual method for handling input
        /// </summary>
        /// <param name="playerInde">PlayerIndex playerIndex</param>
        public virtual void handleInput(PlayerIndex playerIndex) { }


        /// <summary>
        /// Virtual method for drawing
        /// </summary>
        /// <param name="spriteBatchr">SpriteBatch spriteBatch</param>
        public virtual void Draw(SpriteBatch spriteBatch)
        {
        }

        /// <summary>
        /// Sets the objects Vector2 Position
        /// </summary>
        /// <param name="pVector">Vector2</param>
        public void setPosition(Vector2 pVector)
        {
            position = pVector;
        }

        /// <summary>
        /// Sets the objects Vector2 Position
        /// </summary>
        /// <param name="pX">int X Position</param>
        /// <param name="pY">int Y Position</param>
        public void setPosition(int pX, int pY)
        {
            position.X = pX;
            position.Y = pY;
        }

        /// <summary>
        /// Returns the position of the game object.
        /// </summary>
        /// <returns>Vector2</returns>
        public Vector2 getPosition()
        {
            return position;
        }



        /// <summary>
        /// Sets the objects Vector2 Size
        /// </summary>
        /// <param name="sVector">Vector2</param>
        public void setSize(Vector2 sVector)
        {
            size = sVector;
        }

        /// <summary>
        /// Sets the objects Vector2 Size
        /// </summary>
        /// <param name="sX">int X Sizn</param>
        /// <param name="sY">int Y Size</param>
        public void setSize(int sX, int sY)
        {
            size.X = sX;
            size.Y = sY;
        }

        /// <summary>
        /// Returns the size of the game object.
        /// </summary>
        /// <returns>Vector2</returns>
        public Vector2 getSize()
        {
            return size;
        }

    }
}
