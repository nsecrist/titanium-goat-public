/*
 * This code was inspired by code found on http://xnagpa.net/index.php.
 */

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace TitaniumGoat
{

    /// <summary>
    /// Class that is derived from Collection<T>, that stores a collction of ScreenObjects.
    /// </summary>
    public class ScreenObjectManager : Collection<ScreenObject>
    {


        int selectedObject = 0;

        static SpriteFont spriteFont;

        public event EventHandler FocusChanged;


        /// <summary>
        /// Constructor for creating an empty Manager
        /// </summary>
        /// <param name="spriteFont">SpriteFont spriteFont</param>
        public ScreenObjectManager(SpriteFont spriteFont)
            : base()
        {
            ScreenObjectManager.spriteFont = spriteFont;
        }

        /// <summary>
        /// Constructor that creates a manager with a passed in collection as its default list.
        /// </summary>
        /// <param name="sriteFont">SpriteFont spriteFont</param>
        /// <param name="collection">ACollectionClass collection</param>
        public ScreenObjectManager(SpriteFont spriteFont, IList<ScreenObject> collection) :
            base(collection)
        {
            ScreenObjectManager.spriteFont = spriteFont;
        }


        /// <summary>
        /// Updates and handles the input for each ScreenObject in the list.
        /// Updates the object if it is enabled and handles input if the object has focus.
        /// </summary>
        /// <param name="gameTime">GameTime gameTime</param>
        /// <param name="playerIndex">PlayerIndex playerIndex</param>
        public void Update(GameTime gameTime, PlayerIndex playerIndex)
        {
            if (Count == 0)
                return;

            if (!this[selectedObject].isFocus)
                this[selectedObject].isFocus = true;


            foreach (ScreenObject s in this)
            {
                if (s.Enabled)
                    s.Update(gameTime); //Handle animation updates

                if (s.IsFocus)
                    s.handleInput(playerIndex);
            }

            if (KeyboardInputHandler.KeyPressed(Keys.Up))
                PreviousObject();

            if (KeyboardInputHandler.KeyPressed(Keys.Down))
                NextObject();
        }

        /// <summary>
        /// Goes through the list of screen objects and draws each one.
        /// Passes a spriteBatch to each object.
        /// </summary>
        /// <param name="sriteBatch">SpriteBatch spriteBatch</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (ScreenObject s in this)
            {
                    s.Draw(spriteBatch);
            }
        }

        /// <summary>
        /// Goes to the next object in the List if it is Enabled.
        /// For debugging purposes.
        /// </summary>
        public void NextObject()
        {
            if (Count == 0)
                return;

            int currentObject = selectedObject;

            this[selectedObject].isFocus = false;

            do
            {
                selectedObject++;

                if (selectedObject == Count)
                    selectedObject = 0;

               if (this[selectedObject].Enabled)
                {
                    if (FocusChanged != null)
                        FocusChanged(this[selectedObject], null);

                    break;
                }

            } while (currentObject != selectedObject);

            this[selectedObject].isFocus = true;
        }


        /// <summary>
        /// Goes to the previous object in the List if it is Enabled.
        /// For debugging purposes.
        /// </summary>
        public void PreviousObject()
        {
            if (Count == 0)
                return;

            int currentObject = selectedObject;

            this[selectedObject].isFocus = false;

            do
            {
                selectedObject--;

                if (selectedObject < 0)
                    selectedObject = Count - 1;

                if (this[selectedObject].Enabled)
                {
                    if (FocusChanged != null)
                        FocusChanged(this[selectedObject], null);

                    break;
                }
            } while (currentObject != selectedObject);

            this[selectedObject].isFocus = true;
        }


    }
}
