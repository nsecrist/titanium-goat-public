﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace TitaniumGoat
{

    /// <summary>
    /// Class used to define a slider object, for setting a value of some object manually.
    /// </summary>
    class Slider : ScreenObject
    {

        public event EventHandler SelectionChanged;
        Texture2D slider;
        String text = "placeholder";
        int maxItemWidth = 0;
        SpriteFont debugFont;
        double barSlid = 0;
        int maxRight = 700;





        public Slider()
        {

        }


        /// <summary>
        /// Constructor that passes a Texture2d object for the slider texture
        /// </summary>
        /// <param name="stop">Texture2D stop</param>
        public Slider( Texture2D slider)
        {

            this.slider = slider;
        }

        /// <summary>
        /// Loads content for the left/right selection.
        /// </summary>
        /// <param name="theContentManager">ContentManager theContentManager</param>
        /// <param name="theAssetName">string theAssetName</param>
        public void LoadContent(ContentManager theContentManager, string theAssetName)
        {
            debugFont = theContentManager.Load<SpriteFont>(@"Fonts\\DebugFont");
        }

        /// <summary>
        /// Sets the items in the list
        /// </summary>
        /// <param name="theContentManager">ContentManager theContentManager</param>
        /// <param name="theAssetName">int maxWidthe</param>




        /// <summary>
        /// Updates the slider, currently has no use.
        /// </summary>
        /// <param name="gameTime">GameTime gameTime</param>
        public override void Update(GameTime gameTime)
        {
        }

        /// <summary>
        /// Draws the slider.
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch spriteBatch</param>
        public override void Draw(SpriteBatch spriteBatch)
        {
            float itemWidth = debugFont.MeasureString(text).X;
            float textOffset = (itemWidth+maxItemWidth);
            /*
             *             drawTo.X += left.Width;

            float itemWidth = debugFont.MeasureString(items[selectedItem]).X;
            float offset = (maxItemWidth - itemWidth) / 2;

            drawTo.X += offset;

            if (isFocus)
                spriteBatch.DrawString(debugFont, items[selectedItem], drawTo, selectedColor);
            else
                spriteBatch.DrawString(debugFont, items[selectedItem], drawTo, Color.White);

            drawTo.X += -1 * offset + maxItemWidth;*/

            Vector2 drawText = position;

            drawText.X = (position.X - textOffset);


            if (isFocus)
                spriteBatch.DrawString(debugFont, text, drawText, Color.Red);
            else
                spriteBatch.DrawString(debugFont, text, drawText, Color.White);


            Vector2 drawSlider = position;

            float offset;

            offset = (float)((maxRight - position.X) * barSlid);
            drawSlider.X += offset;

            if(isFocus)
                spriteBatch.Draw(slider, drawSlider, Color.Red);

            else
                spriteBatch.Draw(slider, drawSlider, Color.White);


        }

        /// <summary>
        /// Handles input for the slider.
        /// Currently left scrolls left, right scrolls right.
        /// </summary>
        /// <param name="playerIndex">PlayerIndex playerIndex</param>
        public override void handleInput(PlayerIndex playerIndex)
        {
            if (KeyboardInputHandler.KeyDown(Keys.Left))
            {
                barSlid -= .01;
                if (barSlid < 0)
                    barSlid = 0;
                OnSelectionChanged();
            }

            if (KeyboardInputHandler.KeyDown(Keys.Right))
            {
                barSlid += .01;
                if (barSlid > 1)
                    barSlid = 1;
                OnSelectionChanged();
            }
        }

        /// <summary>
        /// For when an event handler is assigned to the object, allows updating of outside methods.
        /// Example: Automatically changes the lighting, instead of having to accept ok.
        /// </summary>
        protected void OnSelectionChanged()
        {
            if (SelectionChanged != null)
            {
                SelectionChanged(this, null);
            }
        }

        /// <summary>
        /// Sets the text of the slider, and spacing between the text and the actual slider
        /// </summary>
        public void setText(string label, int offset)
        {
            text = label;
            maxItemWidth = offset;
        }

        /// <summary>
        /// Sets the Min and Max of the slider placement
        /// </summary>
        /// 
        public void setMax(int b)
        {
            maxRight = b;
        }

        public int getCurValue()
        {
            return (int)(barSlid * 100);
        }
    }
}
