﻿using System.Collections.Generic;

namespace TitaniumGoat
{

    /// <summary>
    /// This static class assists in the storing of debug messages that will be printed on the screen
    /// by the DebugBufferRenderer object.
    /// </summary>
    /// <remarks>Needs lots of work to be usable. Currently under development</remarks>
    public static class DebugBuffer
    {
        private static List<DebugBufferEntry> buffer;
        private static int maxLines = 6;
        public static bool useTimestamps;

        static DebugBuffer()
        {
            buffer = new List<DebugBufferEntry>();
            useTimestamps = false;
        }

        /// <summary>
        /// Write a string to the debug buffer to be written to the screen
        /// </summary>
        /// <param name="output">String to be displayed in the next draw frame</param>
        public static void write(string output)
        {
            if (buffer.Count > maxLines - 1)
            {
                buffer.RemoveAt(0);
            }
            buffer.Add(new DebugBufferEntry(output));

        }
        public static string toString()
        {
            string strReturnable = "";
            foreach (DebugBufferEntry value in buffer)
            {
                if (useTimestamps)
                {
                    strReturnable += value.ToString() + '\n';
                }
                else
                {
                    strReturnable += value.GetMessage() + '\n';
                }
                
            }
            return strReturnable;
        }
        public static void clear()
        {
            buffer = new List<DebugBufferEntry>();
        }
    }
}