﻿using System;

namespace TitaniumGoat
{
    /// <summary>
    /// Defines an entry stored in the DebugBuffer
    /// </summary>
    class DebugBufferEntry
    {
        private DateTime messageTime;
        private string message;
        private string messageWithTimestamp;

        /// <summary>
        /// Creates a new DebugBufferEntry
        /// </summary>
        /// <param name="input">The string that should be displayed in the debug console</param>
        public DebugBufferEntry(string input)
        {
            messageTime = DateTime.Now;
            message = input;
            messageWithTimestamp = String.Format("{0:H:mm:ss.ffff}", messageTime) + "  " + message;
        }

        public override string ToString()
        {
            return messageWithTimestamp;
        }

        /// <summary>
        /// Returns only the message portion of the entry.
        /// </summary>
        /// <returns>Message without the timestamp</returns>
        public string GetMessage()
        {
            return message;
        }
    }
}