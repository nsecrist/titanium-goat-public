﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace TitaniumGoat
{
    /// <summary>
    /// There are two parts to the DebugConsole; the buffer and the renderer.
    /// The renderer handles drawing the contents of the buffer on the screen.
    /// It also handles calculating and displaying the Frames Per Second (FPS) counter
    /// </summary>
    /// <seealso cref="DebugBuffer"/>
    public class DebugBufferRenderer : DrawableGameComponent
    {
        private SpriteBatch spriteBatch;
        private SpriteFont debugFont;
        private Vector2 FontPos;

        // vars for calculating and displaying frame rate
        // source; http://blogs.msdn.com/b/shawnhar/archive/2007/06/08/displaying-the-framerate.aspx
        private int frameRate = 0;
        private int frameCounter = 0;
        private TimeSpan elapsedTime = TimeSpan.Zero;

        /// <summary>
        /// Allows access to a unified SpriteBatch for all level objects
        /// </summary>
        public SpriteBatch SpriteBatch
        {
            get { return spriteBatch; }
        }

        public DebugBufferRenderer(Game game)
            : base(game)
        {
        }

        public override void Initialize()
        {
            DebugBuffer.useTimestamps = true;
            DebugBuffer.write("Debug messages display started...");
            base.Initialize();
        }

        protected override void LoadContent()
        {
            ContentManager content = Game.Content;
            spriteBatch = new SpriteBatch(GraphicsDevice);
            debugFont = content.Load<SpriteFont>(@"Fonts\\DebugFont");
            FontPos = new Vector2(5,5);
        }
        
        public override void Update(GameTime gameTime)
        {
            
            //Only generate FPS if in Debug mode
#if DEBUG
            elapsedTime += gameTime.ElapsedGameTime;
            if (elapsedTime > TimeSpan.FromSeconds(1))
            {
                elapsedTime -= TimeSpan.FromSeconds(1);
                frameRate = frameCounter;
                frameCounter = 0;
            }
#endif
        }
        
        public override void Draw(GameTime gameTime)
        {
#if DEBUG
            //If debugging, render debug text
            spriteBatch.Begin();
            
            spriteBatch.DrawString(debugFont, DebugBuffer.toString(), FontPos + new Vector2(1,1), Color.Black,
                0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0f);
            spriteBatch.DrawString(debugFont, DebugBuffer.toString(), FontPos, Color.LightGreen,
                0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0f);
            // Display frame rate
            frameCounter++;
            string fps = string.Format("fps: {0}", frameRate);
            spriteBatch.DrawString(debugFont, fps, new Vector2(1200, 5), Color.Black,
                                0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0f);
            spriteBatch.DrawString(debugFont, fps, new Vector2(1199, 4), Color.LightGreen,
                                0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0f);
            spriteBatch.End();
#endif
        }
    }
}