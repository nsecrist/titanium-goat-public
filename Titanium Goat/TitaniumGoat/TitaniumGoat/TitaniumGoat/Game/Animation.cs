﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace TitaniumGoat
{

    /// <summary>
    /// A class to handle animations that are stored in sprite sheets
    /// </summary>
    class Animation
    {
        private Vector2 frameCount;
        private Texture2D texture;
        private int frameRate;
        private int currentFrame;
        private bool paused;
        private bool reversed = false;
        private bool loop = true;
        private int lastUpdate;
        private int skipFrames;
 
        //Calculated within object 
        private int totalframes;
        private float updateRate;
        private Vector2 frameSize;

        /// <summary>
        /// Null object/animation purposes. Do not use this
        /// </summary>
        public Animation()
        {
        }

        /// <summary>
        /// Defines the animation settings to be used in animating this graphic
        /// </summary>
        /// <param name="content">
        /// A valid ContentManager object that can be used to load the animation texture.
        /// </param>
        /// <param name="textureName">
        /// The full path of the texture file stored in the TitaniumGoatContent project
        /// </param>
        /// <param name="frames">
        /// An integer greater than 0 that specifies how many frames are contained in your single row animation file.
        /// </param>
        /// <param name="rate">
        /// The number of frames to animate each second (fps)
        /// </param>
        /// <param name="excludedFrames">
        /// [OPTIONAL Default = 0] The number of frames to skip when animating thr sprite. This perameter is useful if your sprite sheet has blank frames at the end.
        /// </param>
        public Animation(ContentManager content, string textureName, Vector2 frames, int rate, int excludedFrames = 0)
        {
            texture = content.Load<Texture2D>(textureName);
            frameCount = frames;
            frameRate = rate;
            paused = false;
            currentFrame = 0;
            skipFrames = excludedFrames;

            //Calculations moved from update method
            totalframes = (int)(frameCount.X * frameCount.Y);
            updateRate = 60 / frameRate;

            int x = texture.Width / (int)frameCount.X;
            int y = texture.Height / (int)frameCount.Y;
            frameSize = new Vector2(x, y);
        }

        /// <summary>
        /// Call on every frame to progress animation at correct rate
        /// </summary>
        public void Update()
        {
            if (!paused)
            {

                //Only make changes to the frame if we meet framerate limits
                if (lastUpdate >= updateRate)
                {
                    if (currentFrame >= totalframes - 1 - skipFrames)
                    {
                        if (loop == true) currentFrame = 0;
                        if (reversed) currentFrame--;
                    }
                    else
                    {
                        if (reversed && currentFrame > 0)
                        {
                            currentFrame--;
                        }
                        else if (!reversed)
                        {
                            currentFrame++;
                        }
                        
                    }
                    lastUpdate = 0;
                }
                else
                {
                    lastUpdate++;
                }
            }
        }

        /// <summary>
        /// Draws the current frame to the specified SpriteBatch
        /// </summary>
        /// <param name="batch">The SpriteBatch that will be used to draw the sprites. The batch must have begun before making a call to this Draw</param>
        /// <param name="drawPosition">The screen position to draw the sprite frame. All sprites are relative to the upper-left pixel of the sprite frame</param>
        /// <param name="scale">The scale at which to draw the frame. 1 is the default, which is 100% scale</param>
        public void Draw(SpriteBatch batch, Vector2 drawPosition, double scale = 1, double transparency = 1)
        {
            int currentRow = currentFrame / (int)frameCount.X;
            int currentColumn = currentFrame - ((int)frameCount.X * currentRow);
            Rectangle subFrame = new Rectangle((int)frameSize.X * currentColumn, (int)frameSize.Y * currentRow, (int)frameSize.X, (int)frameSize.Y);
            batch.Draw(texture, drawPosition, subFrame, Color.White*(float)transparency, 0f, Vector2.Zero, (float) scale, SpriteEffects.None, 0f);
        }


        /// <summary>
        /// Pause or unpause the progression of animation frames
        /// </summary>
        /// <param name="pause">If true, animation will be paused. if false, animation will be unpaused</param>
        public void MakePaused(bool pause)
        {
            paused = pause;
        }

        /// <summary>
        /// Reverses playback of the animation
        /// </summary>
        /// <param name="setReversed">If true, the animation will play backwards. If false, animation will play normally</param>
        public void Reverse(bool setReversed)
        {
            reversed = setReversed;
        }

        /// <summary>
        /// Allows the setting op the loop option
        /// </summary>
        /// <param name="loopAnimation">If true, the animation will return to the first frame after the last frame plays. If false, the animation will pause on the last frame.</param>
        public void Loop(bool loopAnimation)
        {
            loop = loopAnimation;
        }

        /// <summary>
        /// Allows the caller to determine if the animation is on it's last frame
        /// </summary>
        /// <returns>Returns true if the animation is currently set to the last frame (not including excluded frames)</returns>
        public Boolean OnLastFrame()
        {
            bool ret;
            int totalframes = (int)(frameCount.X * frameCount.Y);
            if (currentFrame >= totalframes - 1 - skipFrames)
            {
                ret = true;
            }
            else
            {
                ret = false;
            }
            return ret;
        }

        /// <summary>
        /// Calculates the size of the animation frame
        /// </summary>
        /// <returns>Length and height of the animation frame</returns>
        public Vector2 getTextureSize()
        {
            return frameSize;
        }
    }
}