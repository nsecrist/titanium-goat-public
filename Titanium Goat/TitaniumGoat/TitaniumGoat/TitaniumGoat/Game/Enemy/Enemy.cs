﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace TitaniumGoat
{
    public abstract class Enemy : GameObject
    {

        protected int dir = 0;
        private bool active = true;
        private bool passedPlayer = false;

        protected int center = 700;
        protected int centerL = 571;
        protected int centerR = 845;
        protected int right = 971;

        protected int height = 370;
        protected int startY = 370;
        protected int endY = 658;
        protected int count = 0;
        protected float timeScaling = 0;

        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

        public bool PassedPlayer
        {
            get { return passedPlayer; }
            set { passedPlayer = value; }
        }


        public abstract bool checkCollision(Player player);
  

        /// <summary>
        /// Get type of enemy
        /// </summary>
        public virtual int getState
        {
            get { return 0; }
        }

        public virtual float getScale
        {
            get { return scale; }
        }

        public virtual int getCount
        {
            get { return count; }
        }

        public virtual int getLane
        {
            get { return dir; }
        }

    }
}
