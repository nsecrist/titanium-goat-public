﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace TitaniumGoat
{
    /// <summary>
    /// Enemies that take up only one lane
    /// </summary>
    class laneEnemy : Enemy
    {

        private int level;
        private Animation enemyAnimation = new NullAnimation();

        private ContentManager content;

        /// <summary>
        /// Lane enemy constructor
        /// </summary>
        public laneEnemy()
        {
            position = new Vector2(0, 0);
            velocity = new Vector2(0, 0);
            size = new Vector2(100, 100);
        }

        /// <summary>
        /// Lane enemy constructor
        /// </summary>
        /// <param name="type"> Type of enemy to be created</param>
        /// <param name="theContentManager">For loading enemy texture</param>
        /// <param name="level">level type</param>
        public laneEnemy(int type, float time, ContentManager theContentManager, int level)
        {
            timeScaling = time / 1000 * 60;
            content = theContentManager;
            this.level = level;
            dir = type;
            switch (type)
            {
                case 0:
                    position = new Vector2(centerL, height);
                    break;
                case 1:
                    position = new Vector2(center, height);
                    break;
                case 2:
                    position = new Vector2(centerR, height);
                    break;
            }
            size = new Vector2(200, 1000);
            scale = 0f;
        }

        /// <summary>
        /// Loads the content for enemy
        /// </summary>
        public override void LoadContent()
        {
            int obstacle = GameServices.random.Next(0, 7);
            switch (obstacle)
            {
                case 0:
                    enemyAnimation = new Animation(content, "Obstacles\\Hole", new Vector2(1, 1), 1);
                    break;
                case 1:
                    enemyAnimation = new Animation(content, "Obstacles\\BurningBush", new Vector2(1, 1), 1);
                    break;
                case 2:
                    enemyAnimation = new Animation(content, "Obstacles\\Rock1", new Vector2(1, 1), 1);
                    break;
                case 3:
                    enemyAnimation = new Animation(content, "Obstacles\\Rock2", new Vector2(1, 1), 1);
                    break;
                case 4:
                    enemyAnimation = new Animation(content, "Obstacles\\Rock3", new Vector2(1, 1), 1);
                    break;
                case 5:
                    enemyAnimation = new Animation(content, "Obstacles\\Rock4", new Vector2(1, 1), 1);
                    break;
                case 6:
                    enemyAnimation = new Animation(content, "Obstacles\\Stump", new Vector2(1, 1), 1);
                    break;
                default:
                    enemyAnimation = new Animation(content, "Obstacles\\BeeHive_4x4", new Vector2(4, 4), 8);
                    break;

            }
            textSize = enemyAnimation.getTextureSize();
            scale = (right - centerR) / textSize.X;
        }

        /// <summary>
        /// Updates the enemy
        /// </summary>
        ///  <param name="gameTime">Updates via time, not implemented (60 calls per second)</param>
        public override void Update(GameTime gameTime)
        {
            float d = 0;
            d = (float)(right - centerR) / timeScaling;
            float scaleSize = scale/timeScaling;
            float y = 0;
            y = (float)(endY - startY) / timeScaling;

            enemyAnimation.Update();
            switch (dir)
            {
                case 0:
                    position.X -= d;
                    break;
                case 1:
                    break;
                case 2:
                    position.X += d;
                    break;
            }
            position.Y += y;
            scale += scaleSize;
            count++;

        }

        /// <summary>
        /// Checks if collided with player based off specific actions
        /// </summary>
        /// <param name="player">Checks actions of player</param>
        public override bool checkCollision(Player player)
        {
            bool collide=false;
            if (getLane == player.getLane() && !player.isJumping())
                collide = true;
            return collide;

        }


        
        /// <summary>
        /// Draws the enemy
        /// </summary>
        /// <param name="spritebatch"> spritebatch to draw objects</param>
        public override void Draw(SpriteBatch theSpriteBatch)
        {
            Vector2 drawingPos = new Vector2(position.X - (textSize.X * scale / 2), position.Y - (textSize.Y * scale));
            enemyAnimation.Draw(theSpriteBatch, drawingPos, scale);
        }

        public override float getScale
        {
            get { return scale; }
        }

        public override int getCount
        {
            get { return count; }
        }

        public override int getLane
        {
            get { return dir; }
        }
    }
}
