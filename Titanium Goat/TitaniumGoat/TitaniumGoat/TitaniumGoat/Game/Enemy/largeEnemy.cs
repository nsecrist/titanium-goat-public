﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace TitaniumGoat
{
    /// <summary>
    /// Enemies that take up all lane
    /// </summary>
    class largeEnemy : Enemy
    {


        private int level;
        private Animation enemyAnimation = new NullAnimation();
        private Animation shadow;
        private bool air = false;

        float scaleSize;

        ContentManager content;


        /// <summary>
        /// Large enemy constructor
        /// </summary>
        public largeEnemy()
        {
            position = new Vector2(0, 0);
            velocity = new Vector2(0, 0);
            size = new Vector2(100, 100);
        }


        /// <summary>
        /// Large enemy constructor
        /// </summary>
        /// <param name="type"> Type of enemy to be created</param>
        /// <param name="theContentManager">For loading enemy texture</param>
        /// <param name="level">level type</param>
        public largeEnemy(float time, ContentManager theContentManager, int level)
        {
            timeScaling = time / 1000 * 60;
            content = theContentManager;
            this.level = level;
 
                    position = new Vector2(center, height);

            size = new Vector2(200, 1000);

            
        }

        /// <summary>
        /// Loads the content for enemy
        /// </summary>
        override public void LoadContent()
        {
            int obstacle = GameServices.random.Next(0, 2);
            if (obstacle == 1)
                air = true;
            enemyAnimation = new Animation(content, "Obstacles/log", new Vector2(1, 1), 1);
            shadow = new Animation(content, "Obstacles/shadow", new Vector2(1, 1), 1);
            textSize = enemyAnimation.getTextureSize();
            scale = (right - center) / textSize.X;
        }

        /// <summary>
        /// Updates the enemy
        /// </summary>
        ///  <param name="gameTime">Updates via time, not implemented (60 calls per second)</param>
        override public void Update(GameTime gameTime)
        {
            scaleSize = (right - center);
            scaleSize *= 3f;
            scaleSize /= textSize.X;
            scaleSize /= timeScaling;
            float y = 0;
            y = (float)(endY - startY) / timeScaling;
            
            enemyAnimation.Update();
            position.Y += y;
            scale += scaleSize;
            count++;

        }

        /// <summary>
        /// Checks if collided with player based off specific actions
        /// </summary>
        /// <param name="player">Checks actions of player</param>
        public override bool checkCollision(Player player)
        {
            bool collide = false;
            if (air && !player.isDucking() || !air && !player.isJumping())
                collide = true;
            ScoreManager.increaseCollisionCount();
            return collide;

        }

        /// <summary>
        /// Draws the enemy
        /// </summary>
        /// <param name="spritebatch"> spritebatch to draw objects</param>
        public override void Draw(SpriteBatch theSpriteBatch)
        {
            Vector2 drawingPos;
            if (air)
            {
                drawingPos = new Vector2(700 - (shadow.getTextureSize().X / 2*scale), (position.Y - (shadow.getTextureSize().Y*scale)));
                shadow.Draw(theSpriteBatch, drawingPos, scale, .5f);
                drawingPos = new Vector2(position.X - (textSize.X * scale / 2), 370 - (textSize.Y * scale));
            }
            else
                drawingPos = new Vector2(position.X - (textSize.X * scale / 2), position.Y - (textSize.Y * scale));
            enemyAnimation.Draw(theSpriteBatch, drawingPos, scale);
        }

        public override float getScale
        {
            get { return scale; }
        }

        public override int getCount
        {
            get { return count; }
        }

        public override int getLane
        {
            get { return dir; }
        }
    }
}
