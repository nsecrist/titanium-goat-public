﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace TitaniumGoat
{
    public abstract class GameObject
    {
        protected Vector2 position; //position is bottom, centered oriented
        protected Texture2D texture;
        protected Vector2 velocity;
        protected Vector2 size;
        protected Vector2 textSize;

        protected float scale = 1f;

        private ScreenManager screenManager;

        /// <summary>
        /// A class used to define drawable game objects. Game objects can be
        /// power ups, obstacles, player characters, etc.
        /// </summary>
        public GameObject()
        {
        }

        public virtual void LoadContent() { }
        public virtual void UnloadContent() { }
        public virtual void Update(GameTime gameTime) { }
        public virtual void handleMovement(KeyboardState input) { }
        public virtual void Draw(SpriteBatch theSpriteBatch) {}

        /// <summary>
        /// Sets the objects Vector2 Position
        /// </summary>
        /// <param name="pX">int X Position</param>
        /// <param name="pY">int Y Position</param>
        public void setPosition(int pX, int pY)
        {
            position.X = pX;
            position.Y = pY;
        }

        /// <summary>
        /// Returns the position of the game object.
        /// </summary>
        /// <returns>Vector2</returns>
        public Vector2 getPosition()
        {
            return position;
        }

        /// <summary>
        /// Sets the objects Vector2 Movement
        /// </summary>
        /// <param name="mVector">Vector2</param>
        public void getVelocity(Vector2 mVector)
        {
            velocity = mVector;
        }

        /// <summary>
        /// Sets the objects Vector2 movement
        /// </summary>
        /// <param name="mX">int X movement</param>
        /// <param name="mY">int Y movement</param>
        public void setVelocity(int mX, int mY)
        {
            velocity.X = mX;
            velocity.Y = mY;
        }

        /// <summary>
        /// Returns the movement of the game object.
        /// </summary>
        /// <returns>Vector2</returns>
        public Vector2 getVelocity()
        {
            return velocity;
        }

        /// <summary>
        /// Returns the size of the game object.
        /// </summary>
        /// <returns>Vector2</returns>
        public Vector2 getSize()
        {
            return size;
        }

    }
}
