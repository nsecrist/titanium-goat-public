﻿// ATTRIBUTION: Thanks to Roy Triesscheijn (roy-t@hotmail.com) for helping me put together this code
using Microsoft.Xna.Framework;
using System;

namespace TitaniumGoat
{

    /// <summary>
    /// A class to store game objects that need to be accessible throughout the game
    /// </summary>
    public static class GameServices
    {
        public static Random random = new Random();
        
        private static GameServiceContainer container;
        public static GameServiceContainer Instance
        {
            get
            {
                if (container == null)
                {
                    container = new GameServiceContainer();
                }
                return container;
            }
        }

        /// <summary>
        /// Allows access to game services stored in this static collection
        /// </summary>
        /// <typeparam name="T">A type of GameService or DrawableGameService</typeparam>
        /// <returns>Returns the first instance of a service of the specified type</returns>
        public static T GetService<T>()
        {
            return (T)Instance.GetService(typeof(T));
        }

        /// <summary>
        /// Add a service to the services collection so you can access it anywhere in the project
        /// </summary>
        /// <typeparam name="T">The type of service to be added</typeparam>
        /// <param name="service">Service to be added</param>
        public static void AddService<T>(T service)
        {
            Instance.AddService(typeof(T), service);
        }
    }
}