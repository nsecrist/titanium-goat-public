﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;


namespace TitaniumGoat
{
    public class Level
    {
        private Player player;

        private List<Lane> lanes = new List<Lane>();
        private PersistentScore score;
        private MathHud math;
        private Random random;
        private PowerUps powerUps;
        public int showCollisionOverlay = 0;

        private Texture2D collisionOverlay;

        private int levelTime; //Level time to be set in constructor (based on seconds)
        private bool randomizedLevel = false; // If level is randomized or loaded from file
        private int exit = -1;

        private bool exitReached = false; //If exit has been reached(or time over)

        private int mathCount = 0; // Counter for dodges of enemy
        public int levelType;

        private float enemyTime = 1000;
        private float speed = 1; // No purpose in randomized levels, reduced positions based on location if off the screen
        private float elapsedTime = 0;

        private float randomTime = 0;


        private ContentManager content;

        private static SoundEffect playerHurt;
        private static SoundEffect mathProblemSound;
        private static SoundEffectInstance mathProblemSoundInstance;


        /// <summary>
        /// Level constructor for randomly generated levels
        /// </summary>
        /// <param name="theContentManager">Content manager to load files.</param>
        /// <param name="timer">Time in seconds until the level is finised.</param>
        /// <param name="levelType">Type of level, for changing types of enemies and so on. </param>
        public Level(ContentManager theContentManager, int time, int levelType)//Stream fileStream)
        {
            content = theContentManager;
            levelTime = time;
            this.levelType = levelType;
            randomizedLevel = true;
            random = new Random();

            player = new Player();
            score = new PersistentScore();
            math = new NullMathHud();
            powerUps = new PowerUps(player, this);

            player.LoadContent(content);
            score.LoadContent(content);
            collisionOverlay = theContentManager.Load<Texture2D>(@"Backgrounds\\collisionOverlay");

            playerHurt = content.Load<SoundEffect>("Audio\\Player\\hurt1");
            mathProblemSound = content.Load<SoundEffect>("Audio\\BGM\\mathProblemSound");
            mathProblemSoundInstance = mathProblemSound.CreateInstance();
        }


        /// <summary>
        /// Update all objects contained in level
        /// </summary>
        /// <param name="gametime"> Gametime to update objects based on time</param>
        public void Update(GameTime gametime)
        {
            //Player and score will always be updated
            player.Update(gametime);
            score.Update(gametime);
            powerUps.Update(gametime);
            math.Update(gametime);
            UpdateEnemies(gametime);
            // if level is randomized
            if (randomizedLevel)
            {
                elapsedTime += ((float)gametime.ElapsedGameTime.TotalMilliseconds * speed);
                randomTime += (float)gametime.ElapsedGameTime.TotalMilliseconds;

                timeMethods();

            }
            // else level is loaded from a file
            else
            {
                //Once exit is reached complete level!
                if (exit == 0)
                {
                    exitReached = true;
                    DebugBuffer.write("Exit reached, completing level");
                }
                else
                    exit -= (int)speed;
            }
        }

        private void timeMethods()
        {
            // if 8 seconds remaining stop spawning enemies and if randomTime(time passed) is n seconds generate an enemy
            if (levelTime - (elapsedTime / 1000) >= 8 && randomTime >= (enemyTime + 2000))
            {
                randomTime -= (enemyTime + 2000);
                lanes.Add(new Lane(player.getLane(),enemyTime,content,levelType));
            }

            //if elapsed time has reached level time, game ends
            if (elapsedTime / 1000 >= levelTime)
            {
                exitReached = true;
                DebugBuffer.write("Exit reached, completing level");
            }
        }


        /// <summary>
        /// Update all enemies if they are active
        /// </summary>
        /// <param name="gametime"> Gametime to update objects based on time</param>
        public void UpdateEnemies(GameTime gametime)
        {
            int i;

            foreach (Lane lane in lanes){
                lane.Update(gametime, this);
            }

            //remove lanes not in use to save memory/unneccesary calls
            for (i = 0; i < lanes.Count; i++)
            {
                if (lanes[i].checkRemoval())
                {
                    lanes.RemoveAt(i);
                    i--;
                }
            }
        }

        /// <summary>
        /// Draws lanes containing whatever they have (enemies for now)
        /// </summary>
        /// <param name="spritebatch"> spritebatch to draw objects</param>
        public void DrawLanes(SpriteBatch spritebatch)
        {
            //Goes through the list of enemies
            foreach (Lane lane in lanes)
            {
                lane.Draw(spritebatch);
            }
        }

        /// <summary>
        /// Draws all items stored in level
        /// </summary>
        /// <param name="spritebatch"> spritebatch to draw objects</param>
        public void Draw(SpriteBatch spritebatch)
        {
            player.Draw(spritebatch);
            score.Draw(spritebatch);
            DrawLanes(spritebatch);
            math.Draw(spritebatch);
            if (showCollisionOverlay > 0)
            {
                spritebatch.Draw(collisionOverlay, Vector2.Zero, Color.White);
                showCollisionOverlay--;
            }
        }

        /// <summary>
        /// Returns if level has reached end.
        /// </summary>
        public bool hasFinished()
        {
            return exitReached;
        }

        /// <summary>
        /// Turns a power up on
        /// </summary>
        public void createPowerUp()
        {
            powerUps.randomPowerup(10000);
        }

        /// <summary>
        /// Doubles enemy travel time
        /// </summary>
        public void doubleEnemyTime()
        {

            speed = 0.5F;
            enemyTime *= 2;
        }

        /// <summary>
        /// Halves enemy travel time.
        /// </summary>
        public void halveEnemyTime()
        {
            speed = 1;
            enemyTime /= 2;
        }

        /// <summary>
        /// Lane for objects to send down on the same line.
        /// </summary>
        private class Lane
        {
            private List<Enemy> enemies = new List<Enemy>();

            /// <summary>
            /// Constructs a lane for objects to send down on the same line.
            /// </summary>
            public Lane(int playerLane, float time, ContentManager content, int levelType)
            {
                generateEnemies(playerLane,time,content,levelType);
            }

            /// <summary>
            /// Generate random types of enemies to send down the lane toward the player
            /// </summary>
            /// <param name="playerLane">Player lane to always force them to dodge</param>
            /// <param name="time">"duration" of the enemy</param>
            /// <param name="content">content for loading sprite textures</param>
            /// <param name="levelType">Pass type of level to generate different enemies</param>
            public void generateEnemies(int playerLane, float time, ContentManager content, int levelType)
            {
                int enemyTypes = GameServices.random.Next(0, 2);
                switch (enemyTypes)
                {
                    case 0:
                            int enemyGenerate;
                            do
                            {
                                enemyGenerate = GameServices.random.Next(0, 3);
                            }
                            while (enemyGenerate == playerLane);
                            enemies.Add(new laneEnemy(enemyGenerate, time, content, levelType));
                            enemies.Add(new laneEnemy(playerLane, time, content, levelType));
                        break;
                    case 1:
                        enemies.Add(new largeEnemy(time, content, levelType));
                        break;
                }
                for (int i = 0; i < enemies.Count; i++)
                {
                    enemies[i].LoadContent();
                }
            }

            /// <summary>
            /// Checks to see if the lane needs removed
            /// </summary>
            public bool checkRemoval(){
                bool remove = false;
                for (int i = 0; i < enemies.Count; i++)
                {
                    if (enemies[i].getPosition().Y > 720)
                    {
                        remove = true;
                        break;
                    }
                }

                return remove;
            }

            /// <summary>
            /// Updates the enemies of lane and checks for collisions
            /// </summary>
            /// <param name="playerLane">Player lane to always force them to dodge</param>
            /// <param name="time">"duration" of the enemy</param>
            public void Update(GameTime gameTime, Level level)
            {
                bool collisionOccured = false;
                bool passed = false;
                for (int i = 0; i < enemies.Count; i++)
                {
                    enemies[i].Update(gameTime);
                    if (enemies[i].Active && !enemies[i].PassedPlayer)
                    {
                        if (enemies[i].getCount >= ((level.enemyTime / 1000) * 60))
                        {
                            if (enemies[i].checkCollision(level.player))
                            {
                                collisionOccured = true;
                                enemies[i].Active = false;

                            }
                            passed = true;
                            enemies[i].PassedPlayer = true;

                        }
                    }
                }
                if (collisionOccured)
                {
                    if (level.player.getInvulnerability())
                    {
                        increaseScoreandCount(level);
                    }
                    else
                    {


                        ScoreManager.resetMultiplier();
                        level.showCollisionOverlay = 120;
                        DebugBuffer.write("Collisionoccured do something ");
#if !NO_AUDIO
                        playerHurt.Play();
#endif
                    }
                    for (int h = 0; h < enemies.Count; h++)
                    {
                        enemies[h].Active = false;
                        enemies[h].setPosition(0, 740);
                    }
                    collisionOccured = false;
                }
                else if (passed && !collisionOccured)
                {
                    increaseScoreandCount(level);
                    passed = false;
                    for (int h = 0; h < enemies.Count; h++)
                    {
                        enemies[h].Active = false;
                        enemies[h].setPosition(0, 740);
                    }
                }

            }

            /// <summary>
            /// Draw all enemies in the lane
            /// </summary>
            /// <param name="theSpriteBatch">Spritebatch for drawing enemies</param>
            public void Draw(SpriteBatch theSpriteBatch)
            {
                for (int i = 0; i < enemies.Count; i++)
                {
                    if (enemies[i].Active)
                        enemies[i].Draw(theSpriteBatch);
                }

            }

            /// <summary>
            /// Increase the score and count of the level (when pass an enemy)
            /// </summary>
            /// <param name="level">passing level as reference for calls</param>
            public void increaseScoreandCount(Level level)
            {
                ScoreManager.increaseScore(2000);
                level.mathCount++;
                if (level.mathCount == 2)
                {
                    level.math = new MathHud(level);
#if !NO_AUDIO
                    if (mathProblemSoundInstance.State == SoundState.Stopped)
                    {
                        MediaPlayer.Volume = 0.1f;
                        mathProblemSoundInstance.Play();
                    }
                    else
                    {
                        mathProblemSoundInstance.Resume();
                    }
#endif
                    MediaPlayer.Volume = 1.0f;
                    level.math.LoadContent(level.content);
                    level.mathCount = 0;
                }
            }

        }
    }


}
