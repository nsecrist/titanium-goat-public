﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace TitaniumGoat
{
    public class MathHud : GameObject
    {

        private Vector2 optionTopLeftPos = new Vector2(64, 64);
        private bool drawOptionTopLeft = true;

        private Vector2 optionTopRightPos = new Vector2(1150, 64);
        private bool drawOptionTopRight = true;

        private Vector2 optionBottomLeftPos = new Vector2(64, 600);
        private bool drawOptionBottomLeft = true;

        private Vector2 optionBottomRightPos = new Vector2(1150, 600);
        private bool drawOptionBottomRight = true;

        private Vector2 questionPos = new Vector2(375, 600);
        private Vector2 optionsBGPosOffset = new Vector2(64, 64);

        private int answerAttemps = 1;

        private float maxTime = 8;
        private bool solved = false;
        private bool timeIsUp = false;

        private SpriteFont mathFont;
        private Texture2D problemBG;
        private Texture2D answerBG;

        private float elapsedTime = 0;

        private MathProblem problem;
        private int[] operators;

        private Level level;

        /// <summary>
        /// Constructor for the Math hud(problems)
        /// </summary>
        public MathHud()
        {
            position = new Vector2(1100, 100);
            problem = new MathProblem(ProblemType.addition,Hidden.equals);
            operators = problem.getChoices();
        }

        /// <summary>
        /// Constructor for the Math hud(problems)
        /// </summary>
        public MathHud(Level level)
        {
            this.level = level;
            position = new Vector2(1100, 100);

            //Generate a problem based on level type
            ProblemType pType;
            
            //Ghetto random for problem choice, sets true if seconds are even
            Boolean coinFlip = false;
            if ((DateTime.Now.Second % 2) == 0)
            {
                coinFlip = true;
            }
            switch (this.level.levelType)
            {
                case 0:
                    if (coinFlip)
                    {
                        pType = ProblemType.addition;
                    }
                    else
                    {
                        pType = ProblemType.subtraction;
                    }
                    break;
                case 1:
                    pType = ProblemType.addition;
                    break;
                case 2:
                    if (coinFlip)
                    {
                        pType = ProblemType.addition;
                    }
                    else
                    {
                        pType = ProblemType.subtraction;
                    }
                    break;
                case 3:
                    if (coinFlip)
                    {
                        pType = ProblemType.multiplication;
                    }
                    else
                    {
                        pType = ProblemType.division;
                    }
                    break;
                default:
                    pType = ProblemType.addition;
                    break;
            }
            problem = new MathProblem(pType, Hidden.equals);
            operators = problem.getChoices();
        }

        /// <summary>
        /// Loads all required items for drawing.
        /// </summary>
        public void LoadContent(ContentManager theContentManager)
        {
            mathFont = theContentManager.Load<SpriteFont>(@"Fonts\\JumboFont");
            problemBG = theContentManager.Load<Texture2D>(@"Backgrounds\\Math\\NotePaper");
            answerBG = theContentManager.Load<Texture2D>(@"Backgrounds\\Math\\Note");
        }

        /// <summary>
        /// Updates the math question based on how much time is left and user input.
        /// </summary>
        public override void Update(GameTime gameTime)
        {

            elapsedTime += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            if (elapsedTime / 1000 >= maxTime)
                timeIsUp = true;
            handleInput(PlayerIndex.One);
        }

        /// <summary>
        /// Draws all related materials for the math problem. 
        /// </summary>
        public override void Draw(SpriteBatch theSpriteBatch)
        {
            if (!timeIsUp)
            {
                if (!solved)
                {
                    theSpriteBatch.Draw(problemBG, questionPos - new Vector2(100, 10), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .01f);
                    theSpriteBatch.DrawString(mathFont, problem.getEquation(), questionPos, Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                }
                else
                {
                    theSpriteBatch.Draw(problemBG, questionPos - new Vector2(100, 10), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .01f);
                    theSpriteBatch.DrawString(mathFont, "!!!!! SOLVED !!!!!!", questionPos, Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                }

                if (drawOptionTopLeft)
                {
                    theSpriteBatch.Draw(answerBG, (optionTopLeftPos - optionsBGPosOffset), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .01f);
                    theSpriteBatch.DrawString(mathFont, "" + operators[0], optionTopLeftPos, Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                }
                if (drawOptionTopRight)
                {
                    theSpriteBatch.Draw(answerBG, (optionTopRightPos - optionsBGPosOffset), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .01f);
                    theSpriteBatch.DrawString(mathFont, "" + operators[1], optionTopRightPos, Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                }
                if (drawOptionBottomLeft)
                {
                    theSpriteBatch.Draw(answerBG, (optionBottomLeftPos - optionsBGPosOffset), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .01f);
                    theSpriteBatch.DrawString(mathFont, "" + operators[2], optionBottomLeftPos, Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);

                }
                if (drawOptionBottomRight)
                {
                    theSpriteBatch.Draw(answerBG, (optionBottomRightPos - optionsBGPosOffset), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .01f);
                    theSpriteBatch.DrawString(mathFont, "" + operators[3], optionBottomRightPos, Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                }
            }
        }

        /// <summary>
        /// Handles Player Input (F1-F4 keys) for selecting an answer
        /// Kinect implementation should track specific bones/gestures
        /// </summary>
        public void handleInput(PlayerIndex playerIndex)
        {

            if (!solved)
            {
                if ((KinectInputHandler.gesture.Equals("Select Top Left") || KeyboardInputHandler.KeyPressed(Keys.F1)) && drawOptionTopLeft)
                {
                    solved = problem.isCorrect(operators[0]);
                    if (!solved)
                        drawOptionTopLeft = false;
                    else
                    {
                        drawOptionTopRight = false;
                        drawOptionBottomLeft = false;
                        drawOptionBottomRight = false;
                        answerAttemps++;
                    }

                }
                else if ((KinectInputHandler.gesture.Equals("Select Top Right") || KeyboardInputHandler.KeyPressed(Keys.F2)) && drawOptionTopRight)
                {
                    solved = problem.isCorrect(operators[1]);
                    if (!solved)
                        drawOptionTopRight = false;
                    else
                    {
                        drawOptionTopLeft = false;
                        drawOptionBottomLeft = false;
                        drawOptionBottomRight = false;
                        answerAttemps++;
                    }
                }
                else if ((KinectInputHandler.gesture.Equals("Select Bottom Left") || KeyboardInputHandler.KeyPressed(Keys.F3)) && drawOptionBottomLeft)
                {
                    solved = problem.isCorrect(operators[2]);
                    if (!solved)
                        drawOptionBottomLeft = false;
                    else
                    {
                        drawOptionTopRight = false;
                        drawOptionTopLeft = false;
                        drawOptionBottomRight = false;
                        answerAttemps++;
                    }
                }

                else if ((KinectInputHandler.gesture.Equals("Select Bottom Right") || KeyboardInputHandler.KeyPressed(Keys.F4)) && drawOptionBottomRight)
                {
                    solved = problem.isCorrect(operators[3]);
                    if (!solved)
                        drawOptionBottomRight = false;
                    else
                    {
                        drawOptionTopRight = false;
                        drawOptionBottomLeft = false;
                        drawOptionTopLeft = false;
                        answerAttemps++;
                    }
                }
                if (solved)
                {
                    elapsedTime = 6000;
                    ScoreManager.increaseScore(10000/answerAttemps);
                    ScoreManager.doubleMultiplier();
                    level.createPowerUp();
                }
            }
        }
    }
}
