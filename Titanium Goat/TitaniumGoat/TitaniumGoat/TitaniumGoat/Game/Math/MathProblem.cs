﻿using System;

// TODO:
// ToString needs to be able to return both unanswered and answered forms of the problem [X]
// Choices need to not be allowed to be negative and not be duplicates of previously generated choices [X]
// Documentation [X]
// Correct answer should be in a randomly selected postion of the array [X]
// Difficulty selection [ ] (Not a priority)
// Method which returns bool if chosen answer is correct [X]
// Ability to randomly generate between selected problem types [ ] (Not a priority)

namespace TitaniumGoat
{
    public enum ProblemType { addition, subtraction, multiplication, division };
    public enum Hidden { leftOperand, rightOperand, equals };

    public class MathProblem
    {
        /// <summary>
        /// Variables needed to store the structure and resources of a Math Problem
        /// </summary>
        private int leftOperand, rightOperand, equals;
        private char operation;
        private int[] choices = new int[4];
        private Random randomSeed;
        private ProblemType problemType;
        private Hidden hidden;

        /// <summary>
        /// Default constructor which randomly generates a math problem
        /// </summary>
        public MathProblem()
        {
            randomSeed = new Random();
            generateProblem();
        }

        /// <summary>
        /// Constructor for specifying what type of problem and what type of choices to generate
        /// </summary>
        /// <param name="x">enum ProblemType {addition, subtraction, multiplication, division}</param>
        /// <param name="y">enum Hidden {leftOperand, rightOperand, equals}</param>
        public MathProblem(ProblemType type, Hidden hide)
        {
            randomSeed = new Random();
            generateProblem(type, hide);
        }   

        /// <summary>
        /// Accessor method for returning the choices array
        /// </summary>
        /// <returns>int[4] choices</returns>
        public int[] getChoices()
        {
            return choices;
        }

        // Returns the hidden equation
        public string getEquation()
        {
            switch (hidden)
            {
                case Hidden.leftOperand:
                    string leftHidden = "___" + operation + " " + rightOperand + " = " + equals;
                    return leftHidden;
                case Hidden.rightOperand:
                    string rightHidden = leftOperand + " " + operation + "___" + " = " + equals;
                    return rightHidden;
                case Hidden.equals:
                    string equalsHidden = leftOperand + " " + operation + " " + rightOperand + " = ___";
                    return equalsHidden;
                default:
                    string noneHidden = leftOperand + " " + operation + " " + rightOperand + " = " + equals;
                    return noneHidden;
            }
        }

        /// <summary>
        /// ONLY USED FOR TESTS. Accessor method for the operation
        /// </summary>
        /// <returns>char operation</returns>
        public char getOperator
        {
            get { return operation; }
        }

        /// <summary>
        /// ONLY USED FOR TESTS. Accessor method for returning the leftOperand
        /// </summary>
        /// <returns>int leftOperand</returns>
        public int getLeftOperand
        {
            get { return leftOperand; }
        }

        /// <summary>
        /// ONLY USED FOR TESTS. Accessor method for returning the rightOperand
        /// </summary>
        /// <returns>int rightOperand</returns>
        public int getRightOperand
        {
            get { return rightOperand; }
        }

        /// <summary>
        /// Checks if the players choice is correct or not
        /// </summary>
        /// <param name="playerChoice">int representation of the players answer choice</param>
        /// <returns>True if playerChoice is correct and false otherwise</returns>
        public bool isCorrect(int playerChoice)
        {
            switch (hidden)
            {
                case Hidden.equals:
                    if (playerChoice == equals) { return true; }
                    else return false;
                case Hidden.leftOperand:
                    if (playerChoice == leftOperand) { return true; }
                    else return false;
                case Hidden.rightOperand:
                    if (playerChoice == rightOperand) { return true; }
                    else return false;
                default:
                    return false;
            }
        }

        // Checks to see if the generated problem is valid or not
        private bool isValidProblem()
        {
            if (operation == '/')
            {
                if (leftOperand < rightOperand) return false;
                else if ((leftOperand % rightOperand) != 0) return false;
                else if (rightOperand == 0) return false;
                else return true;
            }
            else if (operation == '-')
            {
                if (leftOperand < rightOperand) return false;
                else return true;
            }
            else
                return true;
        }

        /// <summary>
        /// Select operation/ProblemType by int
        /// </summary>
        /// <param name="input">An integer that maps to a problem type. 1 - addition, 2 subtraction, 3 multiplication, 4 - division</param>
        private void selectOperationByInt(int input)
        {
            switch (input)
            {
                case 1:
                    operation = '+';
                    problemType = ProblemType.addition;
                    break;
                case 2:
                    operation = '-';
                    problemType = ProblemType.subtraction;
                    break;
                case 3:
                    operation = '*';
                    problemType = ProblemType.multiplication;
                    break;
                case 4:
                    operation = '/';
                    problemType = ProblemType.division;
                    break;
            }
        }

        /// <summary>
        /// Setter method for Choices array of size 4. Ensures choices are unique.
        /// </summary>
        /// <param name="potentialChoice1">Potential Choice 1</param>
        /// <param name="potentialChoice2">PotentialChoice 2</param>
        /// <param name="potentialChoice3">PotentialChoice 3</param>
        /// <param name="potentialChoice4">PotentialChoice 4</param>
        private void setChoices(int potentialChoice1, int potentialChoice2, int potentialChoice3, int potentialChoice4)
        {
            choices[0] = potentialChoice1;

            if (potentialChoice2 == choices[0]) { choices[1] = potentialChoice2 + 1; }
            else
                choices[1] = potentialChoice2;
            if (potentialChoice3 == choices[0] || potentialChoice3 == choices[1]) { choices[2] = potentialChoice3 + 3; }
            else
                choices[2] = potentialChoice3;
            if (potentialChoice4 == choices[0] || potentialChoice4 == choices[1] || potentialChoice4 == choices[2]) { choices[3] = potentialChoice4 + 5; }
            else
                choices[3] = potentialChoice4;

            for (int i = 0; i < 3; i++)
            {
                int temp, posToSwap;

                posToSwap = randomSeed.Next(0, 3);

                if (posToSwap != i)
                {
                    temp = choices[posToSwap];
                    choices[posToSwap] = choices[i];
                    choices[i] = temp;
                }
            }
        }

        /// <summary>
        /// Generates the answer choices for the problem
        /// </summary>
        /// <param name="x">Hidden {leftOperand, rightOperand, equals}</param>
        /// <returns>Hidden enum with specifying which value has been hidden</returns>
        private Hidden generateChoices(Hidden hide)
        {

            switch (hide)
            {
                case Hidden.leftOperand:
                    setChoices(leftOperand, randomSeed.Next(leftOperand - 3, leftOperand + 3),
                        randomSeed.Next(leftOperand - 5, leftOperand + 5), randomSeed.Next(1, 10));
                    return Hidden.leftOperand;
                case Hidden.rightOperand:
                    setChoices(rightOperand, randomSeed.Next(rightOperand - 3, rightOperand + 3),
                        randomSeed.Next(rightOperand - 5, rightOperand + 5), randomSeed.Next(1, 10));
                    return Hidden.rightOperand;
                case Hidden.equals:
                    setChoices(equals, randomSeed.Next(equals - 3, equals + 3),
                        randomSeed.Next(equals - 5, equals + 5), randomSeed.Next(equals - 10, equals + 10));
                    return Hidden.equals;
                default:
                    return 0;
            }

        }

        /// <summary>
        /// Evaluates a 
        /// </summary>
        /// <param name="operation">Operator - +, -, *, /</param>
        /// <returns></returns>
        private int evaluate(char operation)
        {
            int result = 0;
            switch (operation)
            {
                case '+':
                    result = leftOperand + rightOperand;
                    break;
                case '-':
                    result = leftOperand - rightOperand;
                    break;
                case '*':
                    result = leftOperand * rightOperand;
                    break;
                case '/':
                    result = leftOperand / rightOperand;
                    break;
            }
            return result;
        }

        /// <summary>
        /// Returns a neatly formated string representation of the problem for debug console with choices. Only used for debug.
        /// </summary>
        /// <param name="hide"></param>
        /// <returns></returns>
        private string toString(Hidden hide)
        {
            switch (hide)
            {
                case Hidden.leftOperand:
                    string leftHidden = "X  " + operation + " " + rightOperand + " = " + equals + "\n"
                        + "With choices: " + choices[0] + ", " + choices[1] + ", " + choices[2] + ", " + choices[3] + "\n"
                        + "Choices are left operands";
                    return leftHidden;
                case Hidden.rightOperand:
                    string rightHidden = leftOperand + " " + operation + "  X" + " = " + equals + "\n"
                        + "With choices: " + choices[0] + ", " + choices[1] + ", " + choices[2] + ", " + choices[3] + "\n"
                        + "Choices are right operands.";
                    return rightHidden;
                case Hidden.equals:
                    string equalsHidden = leftOperand + " " + operation + " " + rightOperand + " =  X" + "\n"
                        + "With choices: " + choices[0] + ", " + choices[1] + ", " + choices[2] + ", " + choices[3] + "\n"
                        + "Choices are answers.";
                    return equalsHidden;
                default:
                    string noneHidden = leftOperand + " " + operation + " " + rightOperand + " = " + equals;
                    return noneHidden;
            }
        }

        /// <summary>
        /// Generates a random problems with a random operator
        /// </summary>
        private void generateProblem()
        {
            int hideMe = randomSeed.Next(1, 3);

            leftOperand = randomSeed.Next(1, 9);
            rightOperand = randomSeed.Next(1, 9);
            selectOperationByInt(randomSeed.Next(1, 5));

            if (isValidProblem())
            {
                equals = this.evaluate(this.operation);
                switch (hideMe)
                {
                    case 1:
                        hidden = generateChoices(Hidden.leftOperand);
                        DebugBuffer.write("Generated problem " + this.toString(Hidden.leftOperand));
                        break;
                    case 2:
                        hidden = generateChoices(Hidden.rightOperand);
                        DebugBuffer.write("Generated problem " + this.toString(Hidden.rightOperand));
                        break;
                    case 3:
                        hidden = generateChoices(Hidden.equals);
                        DebugBuffer.write("Generated problem " + this.toString(Hidden.equals));
                        break;
                    default:
                        break;
                }
            }
            else
                generateProblem();
        }

        /// <summary>
        /// Generates a math problem with a specified type and place to hide
        /// </summary>
        /// <param name="type">ProblemType - type of math problem</param>
        /// <param name="hide">Hidden - part of the equation to be hidden</param>
        private void generateProblem(ProblemType type, Hidden hide)
        {
            leftOperand = randomSeed.Next(1, 11);
            rightOperand = randomSeed.Next(1, 11);
            hidden = hide;

            switch (type)
            {
                case ProblemType.addition:
                    selectOperationByInt(1);
                    equals = leftOperand + rightOperand;
                    generateChoices(hide);
                    break;
                case ProblemType.subtraction:
                    selectOperationByInt(2);
                    if (isValidProblem()) { equals = leftOperand - rightOperand; }
                    else { generateProblem(ProblemType.subtraction,hidden); }
                    generateChoices(hide);
                    break;
                case ProblemType.multiplication:
                    selectOperationByInt(3);
                    equals = leftOperand * rightOperand;
                    generateChoices(hide);
                    break;
                case ProblemType.division:
                    selectOperationByInt(4);
                    if (isValidProblem()) { equals = leftOperand / rightOperand; }
                    else { generateProblem(ProblemType.division, hidden); }
                    generateChoices(hide);
                    break;
                default:
                    generateProblem();
                    break;
            }

            DebugBuffer.write("Generated problem " + this.toString(hide));
        }

    }
}
