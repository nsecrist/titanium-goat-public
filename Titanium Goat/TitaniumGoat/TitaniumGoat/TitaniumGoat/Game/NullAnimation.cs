﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace TitaniumGoat
{

    /// <summary>
    /// Null default animation object
    /// </summary>
    class NullAnimation : Animation
    {
        public NullAnimation()
        {
        }

        new public void Update()
        {
        }

        new public void Draw(SpriteBatch batch, Vector2 drawPosition, double scale = 1, double transparency = 1)
        {
        }

        new public void MakePaused(bool pause)
        {
        }


        new public void Reverse(bool setReversed)
        {
        }

 
        new public void Loop(bool loopAnimation)
        {
        }

 
        new public Boolean OnLastFrame(){
            return true;
        }

 
        new public Vector2 getTextureSize()
        {
            return new Vector2(0,0);
        }
    }
}