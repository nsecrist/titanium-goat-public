using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using TitaniumGoatKinectLib;

namespace TitaniumGoat
{
    public class Player : GameObject
    {
        private enum StateOptions {running=0, jumping=1, ducking=2}
        private Animation runningAnimation = new NullAnimation();
        private Animation jumpingAnimation = new NullAnimation();
        private Animation duckingAnimation = new NullAnimation();

        private StateOptions curState = StateOptions.running;
        private bool invulnerable = false;

        int leftLane = 428; 
        int middleLane = 695;
        int rightLane = 962;
        int lane = 1;
        bool slanted = false;
        bool jumping = false;
        bool duck = false;

        /// <summary>
        /// Default player constructor
        /// </summary>
        public Player()
        {
            position = new Vector2(600, 656);
            velocity = new Vector2(0, 0);
            size = new Vector2(200, 1000);
        }

        /// <summary>
        /// Player constructor
        /// </summary>
        /// <param name="theContentManager"> Content manager for loading</param>
        public void LoadContent(ContentManager theContentManager)
        {
            runningAnimation = new Animation(theContentManager, @"Characters\\Monkey", new Vector2(3,3), 32);
            jumpingAnimation = new Animation(theContentManager, @"Characters\\Monkey", new Vector2(3, 3), 32);
            duckingAnimation = new Animation(theContentManager, @"Characters\\Monkey", new Vector2(3, 3), 32);
            textSize = runningAnimation.getTextureSize();
        }

        /// <summary>
        /// Update the player
        /// </summary>
        /// <param name="gameTime"> gameTime for updating</param>
        override public void Update(GameTime gameTime) {
            
            //Update character animation
            switch (curState)
            {
                case StateOptions.running:
                    runningAnimation.Update();
                    break;
                case StateOptions.jumping:
                    jumpingAnimation.Update();
                    break;
                case StateOptions.ducking:
                    duckingAnimation.Update();
                    break;
            }
            

            updateMovement();
        }

        /// <summary>
        /// Draws the player animation
        /// </summary>
        /// <param name="theSpriteBatch"> sprite batch for drawing</param>
        public override void Draw(SpriteBatch theSpriteBatch)
        {
            Vector2 drawingPos = new Vector2(position.X - (textSize.X / 2*scale), position.Y - (textSize.Y * scale));

            switch (curState){
                case StateOptions.running:
                    runningAnimation.Draw(theSpriteBatch, drawingPos,scale,.5);
                    break;
                case StateOptions.jumping:
                    jumpingAnimation.Draw(theSpriteBatch, drawingPos,scale,.5);
                    break;
                case StateOptions.ducking:
                    duckingAnimation.Draw(theSpriteBatch, drawingPos,scale,.5);
                    break;
            }
            
        }

        public void jump()
        {
        }

        public void handleXmovement()
        {
        }

        public void updateMovement()
        {
            int positionX = (int)getPosition().X,positionY= (int)getPosition().Y;

            if (curState != StateOptions.jumping)
            {
                if (KinectInputHandler.checkIfJump())
                {
                    curState = StateOptions.jumping;
                    setVelocity(0, 25);
                    jumping = true;
                    scale = 1f;
                }

                else if (KinectInputHandler.gesture.Equals("Duck"))
                {
                    curState = StateOptions.ducking;
                    duck = true;
                    textSize = duckingAnimation.getTextureSize();
                    scale = 0.5f;
                }

                else
                {
                    duck = false;
                    curState = StateOptions.running;
                    textSize = runningAnimation.getTextureSize();
                    scale = 1f;
                }
            }

            if (curState == StateOptions.jumping)
            {
                positionY -= (int)getVelocity().Y;
                setVelocity(0, (int)getVelocity().Y - 1);
                if (positionY >= 656)
                {
                    setVelocity(0, 0);
                    jumping = false;
                    curState = StateOptions.running;
                }
            }


            else
            {
                if (GameServices.GetService<KinectChooser>().Sensor == null || !KinectInputHandler.skeletonTracked)
                {
                    MouseState ms = Mouse.GetState();
                    positionX = ms.X;
                }
                else
                {
                    positionX = KinectInputHandler.shoulderCenterX;
                }

                int change = KinectInputHandler.shoulderCenterX - KinectInputHandler.hipCenterX;
                int check = Math.Abs(change);
                if (check > 50)
                    slanted = true;
                else
                    slanted = false;
                if (slanted)
                {
                    if (change < -50) //prev 427
                    {
                        positionX = leftLane;
                        lane = 0;
                    }
                    else if (change > 50) //prev 853
                    {
                        positionX = rightLane;
                        lane = 2;
                    }
                    else
                    {
                        positionX = middleLane;
                        lane = 1;
                    }
                }
                else
                {
                    if (positionX < 450) //prev 427
                    {
                        positionX = leftLane;
                        lane = 0;
                    }
                    else if (positionX >= 830) //prev 853
                    {
                        positionX = rightLane;
                        lane = 2;
                    }
                    else
                    {
                        positionX = middleLane;
                        lane = 1;
                    }
                }
            }
            setPosition(positionX, positionY);
        }
        /// <summary>
        /// Returns the state of the player
        /// </summary>
        public int getState()
        {
            return (int) curState;
        }

        /// <summary>
        /// Sets the state of the player, testing purposes
        /// </summary>
        public void setState(int state)
        {
            curState = (StateOptions) state;
        }

        public int getLane()
        {
            return lane;
        }

        public void setLane(int lane)
        {
            this.lane = lane;
        }

        public int autoPilot()
        {
            int x = 0;
            switch (lane)
            {
                case 0:
                    x = leftLane;
                    break;
                case 1:
                    x = middleLane;
                    break;
                case 2:
                    x = rightLane;
                    break;
            }
            return x;
        }



        public bool getInvulnerability()
        {
            return invulnerable;
        }

        public void reverseVulnerability()
        {
            invulnerable = !invulnerable;
        }


        public bool isJumping()
        {
            return jumping;
        }

        public bool isDucking()
        {
            return duck;
        }
    }
}
