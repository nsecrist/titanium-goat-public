﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TitaniumGoat
{


    class PowerUps
    {
        private int[] times = new int[4]; // 0 -Bullet time 1 - Multiplier 2- AutoPilot 3- god mode
        private bool[] powerUps = new bool[4]; // 0 -Bullet time 1 - Multiplier 2- AutoPilot 3- god mode
        private int []elapsedtimes = new int[4];
        Player player;
        Level level;

        /// <summary>
        /// Power ups constructer that stores quick references of active objects in game
        /// </summary>
        /// <param name="player">Player for modifying some paramters</param>
        /// <param name="level">Level for modifying some paramters</param>
        public PowerUps(Player player, Level level)
        {
            this.level = level;
            this.player = player;
        }

        /// <summary>
        /// Counts down time until power up is cancelled, if it is active
        /// </summary>
        /// <param name="gameTime">Game time to get elapsed time</param>
        public virtual void Update(GameTime gameTime)
        {
            for (int i = 0; i < times.Length; i++)
            {
                if (powerUps[i])
                {
                    elapsedtimes[i] += gameTime.ElapsedGameTime.Milliseconds;
                    if (elapsedtimes[i] >= times[i])
                    {
                        elapsedtimes[i] = 0;
                        powerUps[i] = false;
                        cancelPowerUp(i);
                        DebugBuffer.write("Power up " +i+" cancelled");
                    }
                }
            }
        }

        /// <summary>
        /// Drawing to be perfomed if future implementation of visual aid is needed
        /// </summary>
        public void Draw(SpriteBatch theSpriteBatch)
        {
        }

        /// <summary>
        /// Selects a random power up to be turned on
        /// </summary>
        /// <param name="time">Duration of power up, resets current one if active</param>
        public void randomPowerup(int time)
        {
            int type = GameServices.random.Next(0,3);
            times[type] = time;
            if (!powerUps[type])
            {
                powerUps[type] = true;
                performPowerUp(type);
            }
            DebugBuffer.write("Power up type = " + type);
        }

        // 0 -Bullet time 1 - Multiplier 2- AutoPilot 3- God mode
        /// <summary>
        /// Sets the power up on
        /// </summary>
        /// <param name="type">Type of powerup being performed</param>
        private void performPowerUp(int type)
        {
            switch (type)
            {
                case 0:
                    DebugBuffer.write("Slow mo" + type);
                    level.doubleEnemyTime();
                    break;
                case 1:
                    DebugBuffer.write("Double multiplier" + type);
                    ScoreManager.doubleBonusMultiplier();
                    break;
                case 2:
                    DebugBuffer.write("God mode =" + type);
                    player.reverseVulnerability();
                    break;
            }
        }

        // 0 -Bullet time 1 - Multiplier 2- AutoPilot 3- invulnerability
        /// <summary>
        /// Turns off the power up
        /// </summary>
        /// <param name="type">Type of powerup being performed</param>
        private void cancelPowerUp(int type)
        {
            switch (type)
            {
                case 0:
                    level.halveEnemyTime();
                    break;
                case 1:
                    ScoreManager.halveBonusMultiplier();
                    break;
                case 2:
                    player.reverseVulnerability();
                    break;
            }
        }
    }
}
