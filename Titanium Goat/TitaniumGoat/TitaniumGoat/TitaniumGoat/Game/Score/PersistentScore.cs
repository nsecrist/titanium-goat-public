﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace TitaniumGoat
{
    /// <summary>
    /// Draws a representation of the score object on the screen using a sprite based font.
    /// </summary>
    public class PersistentScore : GameObject
    {

        //Images for score difits
        private Texture2D[] numberTextures;

        /// <summary>
        /// Constructor for displaying the score on a screen
        /// </summary>
        public PersistentScore()
        {
            position = new Vector2(640, 50);
            velocity = Vector2.Zero;
            numberTextures = new Texture2D[10];

        }

        /// <summary>
        /// Loads the content needed for drawing
        /// </summary>
        public void LoadContent(ContentManager theContentManager)
        {
            //Load number graphics
            for (int i = 0; i < 10; i++)
            {
                numberTextures[i] = theContentManager.Load<Texture2D>(@"Levels\\numbers\\" + i);
            }
        }

        override public void Update(GameTime gameTime)
        {
            //Nobody here but us chickens!
        }

        /// <summary>
        /// Draws the current score
        /// </summary>
        public override void Draw(SpriteBatch theSpriteBatch)
        {
            //Build and draw the sprite based score display
            int characterOffset = 70;
            string currentScore = ScoreManager.currentScore.ToString();
            int centeroffset = (int)position.X - (characterOffset * currentScore.Length)/2;
            for (int i = 0; i < currentScore.Length; i++)
            {
                int nextNumber = int.Parse(currentScore.Substring(i,1));
                if (nextNumber < 10)
                {
                    theSpriteBatch.Draw(numberTextures[nextNumber], new Rectangle((int) centeroffset + (i*characterOffset), (int)position.Y, 71, 100), Color.White);

                }
            }
        }

    }
}
