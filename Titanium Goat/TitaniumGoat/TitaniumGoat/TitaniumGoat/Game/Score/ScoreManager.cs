﻿//-----------------------------------------------------------------------------
// This code was created using an XNA sample code project as inspiration.
// You can find the original sample code at this link;
// http://www.pacificsimplicity.ca/blog/loading-and-save-highscore-xmlisolatedstorage-tutorial
// Also see: http://msdn.microsoft.com/en-us/library/bb203924(v=xnagamestudio.40).aspx
// Somethings are pending change on Kinect Integration. 
//-----------------------------------------------------------------------------

using System;
using System.IO;
using System.Xml.Serialization;

namespace TitaniumGoat
{
    /// <summary>
    /// Handles loading and saving of gamedata - highscores
    /// </summary>
    public static class ScoreManager
    {
        //Initialize some variables for the class
        private static int PlayerScore = 0;
        private static string PlayerImagePath;
        private static int collisonCount;
        private static double currentMultiplier = 1;
        private static double bonusMultiplier = 1;
        private static bool isHighscore = false;

        private static int currentLevel = LevelSelectScreen.getlevelSelection;
        private static readonly string HighScoresFilename = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "TitaniumGoat", "highscores.xml");
        private const int maxHighScores = 5;

        /// <summary>
        /// Define structure for HighScore data
        /// </summary>
        /// /// <param name="size">If size is given, the sizeOfHighScoreData of the array will be defined.</param>
        [Serializable]
        public struct HighScoreData
        {
            public string[] PlayerImage;
            public int[] Score;
            public int[] Level;
            public int[] Collisions;

            public int sizeOfHighScoreData;
            public HighScoreData(int size)
            {
                PlayerImage = new string[size];
                Score = new int[size];
                Level = new int[size];
                Collisions = new int[size];
                sizeOfHighScoreData = size;
            }
        }

        /// <summary>
        /// Returns the current ScoreManager.
        /// </summary>
        /// <returns></returns>
        public static int currentScore
        {
            get {return PlayerScore;}
        }

        /// <summary>
        /// Sets name of path to store highscore image
        /// </summary>
        /// <param name="a"></param>
        public static string ImagePath
        {
            set{ PlayerImagePath = value;}
        }

        /// <summary>
        /// Resets Score counters and booleans. Should be called at game end or start
        /// </summary>
        public static void reset()
        {
            PlayerScore = 0;
            collisonCount = 0; 
            PlayerImagePath = null; 
            currentMultiplier = 1;
            isHighscore = false;
        }

        /// <summary>
        /// Increases score by a specified increment
        /// </summary>
        /// <param name="a">The amount to increase score by</param>
        public static void increaseScore(int increase)
        {
            PlayerScore += (int)(increase * currentMultiplier * bonusMultiplier);
        }

        /// <summary>
        /// Increases collison count. Used to determine number of "stars" at round it. Should be called every collision.
        /// </summary>
        public static int getCollisionCount
        {
            get { return collisonCount; }
        }

        /// <summary>
        /// Increases collison count. Used to determine number of "stars" at round it. Should be called every collision.
        /// </summary>
        public static void increaseCollisionCount()
        {
            collisonCount++;
        }

        /// <summary>
        /// Increase currentMultiplier by specified value
        /// </summary>
        /// <param name="addend">Double to inrease currentMultiplier by</param>
        public static void increaseMultiplier(double addend)
        {
            currentMultiplier += addend;
        }

        /// <summary>
        /// Double current multipler
        /// </summary>
        public static void doubleMultiplier()
        {
            if (currentMultiplier < 8)
            {
                currentMultiplier *= 2;
            }
        }

        /// <summary>
        /// Reset multipler
        /// </summary>
        public static void resetMultiplier()
        {
            currentMultiplier = 1;
        }

        /// <summary>
        /// Double value of bonus multipler
        /// </summary>
        public static void doubleBonusMultiplier(){
            bonusMultiplier *= 2;
        }

        /// <summary>
        /// Reduce value of bonus multiplier by half
        /// </summary>
        public static void halveBonusMultiplier()
        {
            bonusMultiplier /= 2;
        }

        /// <summary>
        /// Checks to see if current score is highscore or not
        /// </summary>
        /// <returns>True if current score is highscore; False if it is not</returns>
        public static bool isNewHighScore
        {
            get {return isHighscore;}
        }

        /// <summary>
        /// Generates a string concatenating data from the highscores file
        /// </summary>
        /// <returns></returns>
        public static string getHighScoresAsString()
        {
            // Create the data to save
            HighScoreData data2 = Load();

            // Create scoreBoardString buffer
            String scoreBoardString = "";

            for (int i = 0; i < data2.sizeOfHighScoreData; i++)
            {
                scoreBoardString = String.Format("{0, 0} - {1, 10} \n", scoreBoardString, data2.Score[i]);
            }

            return scoreBoardString;
        }

        /// <summary>
        /// Save score to highscore file provided that PlayerScore is one that is eligible for a highscore. Typically, this should be called at the end of a game.
        /// </summary>
        public static void Save()
        {
            // Create the data to save
            HighScoreData data = Load();
            data = checkEndScore(data);

            if (isHighscore)
            {
                SaveHighScoresFile(data, HighScoresFilename);
            }
        }

        /// <summary>
        /// Loads highscores file
        /// </summary>
        /// <returns>HighScoreData structure with loaded information</returns>
        public static HighScoreData Load()
        {
            if (!File.Exists(HighScoresFilename))
            {
                createDefaultXMLFile();
            }

            HighScoreData data = new HighScoreData(maxHighScores);
            // Open the file as read-only. If it doesn't exist, create it. 
            using (FileStream stream = File.Open(HighScoresFilename, FileMode.Open))
            {
                try
                {
                    // Read the data from the file
                    XmlSerializer serializer = new XmlSerializer(typeof(HighScoreData));
                    data = (HighScoreData)serializer.Deserialize(stream);
                }
                catch (Exception e)
                {
                    DebugBuffer.write(e.ToString());
                }
            }

            return (data);
        }

        /// <summary>
        /// Saves highscores file. If the file does not exist, it will be created.
        /// </summary>
        /// <param name="data">HighScoreData structure</param>
        /// <param name="filename">Path of highscores file</param>
        private static void SaveHighScoresFile(HighScoreData data, string filename)
        {
            if (File.Exists(HighScoresFilename))
            {
                File.Delete(HighScoresFilename);
            }

            // Open the file, creating it if necessary
            using (FileStream stream = File.Open(HighScoresFilename, FileMode.OpenOrCreate))
            {
                // Convert the object to XML data and put it in the stream
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(HighScoreData));
                    serializer.Serialize(stream, data);
                }
                catch (Exception e)
                {
                    DebugBuffer.write(e.ToString());
                }
            }
        }

        /// <summary>
        /// Checks whether the given HighScoreData structure is worth of a new highscore and replaces them if necessary
        /// </summary>
        /// <param name="data">HighScoreData to compare to current saved scores</param>
        /// <returns>Updated HighScoresData with new highscores</returns>
        private static HighScoreData checkEndScore(HighScoreData data)
        {
            //Determine where in the array of current highscores the submitted score would go
            int scoreIndex = -1;
            for (int i = 0; i < data.sizeOfHighScoreData; i++)
            {
                if (PlayerScore >= data.Score[i])
                {
                    scoreIndex = i;
                    break;
                }
            }

            if (scoreIndex > -1)
            {
                //New high score found ... do swaps
                isHighscore = true;
                for (int i = data.sizeOfHighScoreData - 1; i > scoreIndex; i--)
                {
                    data.PlayerImage[i] = data.PlayerImage[i - 1];
                    data.Score[i] = data.Score[i - 1];
                    data.Level[i] = data.Level[i - 1];
                    data.Collisions[i] = data.Collisions[i - 1];
                }

                data.PlayerImage[scoreIndex] = PlayerImagePath;
                data.Score[scoreIndex] = PlayerScore;
                data.Level[scoreIndex] = currentLevel + 1;
                data.Collisions[scoreIndex] = collisonCount;
            }
            return data;
        }

        /// <summary>
        /// Checks if a highscorefile already exists if not creates one with mock values
        /// </summary>
        /// Considered refactoring with loading a file from XNA Content but using the XNA Content and TitianiumGoat namespace isn't worth the overhead.
        private static void createDefaultXMLFile()
        {

            using (FileStream create = File.Create(HighScoresFilename)) { };

                HighScoreData data = new HighScoreData(maxHighScores);

                data.PlayerImage[0] = "highscore_00-00-0000_00-00-00-PM.png";
                data.Level[0] = 3;
                data.Score[0] = 5000;
                data.Collisions[0] = 0;

                data.PlayerImage[1] = "highscore_00-00-0000_00-00-00-PM.png";
                data.Level[1] = 2;
                data.Score[1] = 4000;

                data.PlayerImage[2] = "highscore_00-00-0000_00-00-00-PM.png";
                data.Level[2] = 3;
                data.Score[2] = 3000;
                data.Collisions[0] = 1;

                data.PlayerImage[3] = "highscore_00-00-0000_00-00-00-PM.png";
                data.Level[3] = 2;
                data.Score[3] = 2000;
                data.Collisions[0] = 3;

                data.PlayerImage[4] = "highscore_00-00-0000_00-00-00-PM.png";
                data.Level[4] = 1;
                data.Score[4] = 1000;
                data.Collisions[0] = 5;

                SaveHighScoresFile(data, HighScoresFilename);
        }
    }
}
