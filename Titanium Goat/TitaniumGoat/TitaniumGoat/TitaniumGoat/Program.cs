namespace TitaniumGoat
{

#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        static void Main(string[] args)
        {

            //Creates a new instance of the game and runs it
            using (TitaniumGoatGame game = new TitaniumGoatGame())
            {
                game.Run();
            }
        }
    }
#endif
}