﻿//-----------------------------------------------------------------------------
// This code was created using an XNA sample code project as inspiration.
// However, it only somewhat resembles the original code.
// You can find the original sample code at this link;
// http://xbox.create.msdn.com/en-US/education/catalog/sample/game_state_management
// Licence = Microsoft Public Licence (MS-PL)
//-----------------------------------------------------------------------------

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using TitaniumGoatKinectLib;

namespace TitaniumGoat
{
    /// <summary>
    /// Main game level
    /// </summary>
    public class GameplayScreen : GameScreen
    {
        // Specifies the level type to use
        private int levelType;

        private ContentManager content;
        private float pauseAlpha;

        // Bootstrapping the old level data into this new format.
        protected Texture2D texBackground;
        protected Vector2 camera = new Vector2(0, 0);
        protected Vector2 cameraOffset = new Vector2(1280, 0);

        private Level level;

        private int dx = 1;

        /// <summary>
        /// Creates a new level of the specified type
        /// </summary>
        /// <param name="inputLevelType">Level type integer</param>
        public GameplayScreen(int inputLevelType)
        {
            // Use input to set level type
            levelType = inputLevelType;
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
        }

        /// <summary>
        /// Load graphics content for the game.
        /// </summary>
        public override void LoadContent()
        {
            if (content == null)
                content = new ContentManager(ScreenManager.Game.Services, "Content");
            
            // Select background based on level type
            switch (levelType)
            {
                case 1:
                    texBackground = content.Load<Texture2D>(@"Backgrounds\\forest");
                    break;
                case 2:
                    texBackground = content.Load<Texture2D>(@"Backgrounds\\desert");
                    break;
                case 3:
                    texBackground = content.Load<Texture2D>(@"Backgrounds\\volcano");
                    break;
                default:
                    texBackground = content.Load<Texture2D>(@"Backgrounds\\forest");
                    break;
            }

            level = new Level(content, 180, levelType);


            
            // once the load has finished, we use ResetElapsedTime to tell the game's
            // timing mechanism that we have just finished a very long frame, and that
            // it should not try to catch up.
            ScreenManager.Game.ResetElapsedTime();
        }

        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void UnloadContent()
        {
            content.Unload();
        }

        /// <summary>
        /// Updates the state of the game. This method checks the GameScreen.IsActive
        /// property, so the game will stop updating when the pause menu is active,
        /// or if you tab away to a different application.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);

            // Gradually fade in or out depending on whether we are covered by the pause screen.
            if (coveredByOtherScreen)
                pauseAlpha = Math.Min(pauseAlpha + 1f / 32, 1);
            else
                pauseAlpha = Math.Max(pauseAlpha - 1f / 32, 0);

            if((!IsActive && pauseAlpha == 0))
            {
                ScreenManager.AddScreen(new PauseScreen(), null);
            }

            if (IsActive)
            {
                level.Update(gameTime);
                scrollBG();

                //Check to see if game has ended
                KeyboardState currentKeyboardState = Keyboard.GetState();
                if (level.hasFinished() || KeyboardInputHandler.KeyPressed(Keys.Escape))
                {
                    IsExiting = true;
                }
                //Check to see if user wants to pause
                if (KeyboardInputHandler.KeyPressed(Keys.Tab) && pauseAlpha == 0)
                {
                    // Display the pause screen over the current screen
                    DebugBuffer.write("Creating pause menu...");
                    ScreenManager.AddScreen(new PauseScreen(), null);
                }

                if (GameServices.GetService<KinectChooser>().getStatus() == "Connected" && KinectInputHandler.skeletonTracked == false && pauseAlpha == 0)
                {
                    // Display the pause screen over the current screen
                    DebugBuffer.write("Lost skeleton! Pausing...");
                    ScreenManager.AddScreen(new LostSkeletonTrackingScreen(), null);
                }

            }

            if (ScreenState == ScreenState.Exited)
            {
                ScoreManager.Save();
                if (ScoreManager.isNewHighScore == true && GameServices.GetService<KinectChooser>().getStatus() == "Connected")
                {
                    DebugBuffer.write("Going to highscore take picture");
                    ScreenManager.AddScreen(new HighscoreTakePicture(), null);
                    ScreenManager.RemoveScreen(this);
                }
                else
                {
                    DebugBuffer.write("Going to highscore display2");
                    ScreenManager.AddScreen(new HighscoresDisplay(), null);
                    ScreenManager.RemoveScreen(this);
                }
            }

        }

        /// <summary>
        /// Draws the gameplay screen.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            ScreenManager.GraphicsDevice.Clear(ClearOptions.Target,
                                               Color.Black, 0, 0);
            
            // Get a copy of the shared SpriteBatch
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);

            spriteBatch.Draw(texBackground, Vector2.Zero, null, Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 1);

            level.Draw(spriteBatch);
            spriteBatch.End();

            if (TransitionPosition > 0 || pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);

                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }

        private void scrollBG()
        {
            camera.X = camera.X - dx;
            if (camera.X == -1280)
                camera.X = 0;
            cameraOffset.X = cameraOffset.X - dx;
            if (cameraOffset.X == 0)
                cameraOffset.X = 1280;
        }
    }
}
