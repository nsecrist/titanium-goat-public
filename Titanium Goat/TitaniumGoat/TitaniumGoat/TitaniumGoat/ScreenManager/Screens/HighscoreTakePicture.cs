﻿//-----------------------------------------------------------------------------
// This code was created using an XNA sample code project as inspiration.
// However, it only somewhat resembles the original code.
// You can find the original sample code at this link;
// http://xbox.create.msdn.com/en-US/education/catalog/sample/game_state_management
// Licence = Microsoft Public Licence (MS-PL)
//-----------------------------------------------------------------------------

using System;
using System.IO;
using Microsoft.Kinect;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using TitaniumGoatKinectLib;


namespace TitaniumGoat
{
    /// <summary>
    /// Highscores Display (HSD) at the end of the game
    /// Also takes picture if played game is a high score
    /// </summary>
    public class HighscoreTakePicture : GameScreen
    {
        private ContentManager content;

        private SpriteFont scoreFont;
        private Vector2 scoreFontPos = new Vector2(650, 200);

        private Texture2D colorVideo;
        private float pictureCountdown;
        private float timeElapsed;
        private bool pictureTaken = false;

        private static string picturefilename = string.Format("highscore_{0:MM-dd-yyyy_hh-mm-ss-tt}.png", DateTime.Now);

        /// <summary>
        /// Constructor for HighscoreTakePicture Screen
        /// </summary>
        public HighscoreTakePicture()
        {
            ScoreManager.ImagePath = picturefilename;
            ScoreManager.Save();

            KinectChooser kinect = GameServices.GetService<KinectChooser>();
            GraphicsDeviceManager graphics = GameServices.GetService<GraphicsDeviceManager>();

            //Enable color stream and register event hangler for ColorImageFrameReady
            if (kinect.Sensor != null && ScoreManager.isNewHighScore == true)
            {
                kinect.Sensor.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);
                kinect.Sensor.ColorFrameReady += new EventHandler<ColorImageFrameReadyEventArgs>(kinect_ColorFrameReady);
                colorVideo = new Texture2D(graphics.GraphicsDevice, kinect.Sensor.ColorStream.FrameWidth, kinect.Sensor.ColorStream.FrameHeight);
            }
        }

        /// <summary>
        /// Load content for HSD
        /// </summary>
        public override void LoadContent()
        {
            if (content == null)
            {
                content = new ContentManager(ScreenManager.Game.Services, "Content");
            }

            scoreFont = content.Load<SpriteFont>(@"Fonts\\ComicSansMS");

            // once the load has finished, we use ResetElapsedTime to tell the game's
            // timing mechanism that we have just finished a very long frame, and that
            // it should not try to catch up.
            ScreenManager.Game.ResetElapsedTime();
        }

        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void UnloadContent()
        {
            content.Unload();
        }


        /// <summary>
        /// Updates the state of the game. This method checks the GameScreen.IsActive
        /// property, so the game will stop updating when the pause menu is active,
        /// or if you tab away to a different application.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);

            //Increase the pictureCount as gametime increases
            pictureCountdown += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            timeElapsed += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (pictureTaken == true)
            {
                IsExiting = true;
            }

            // If the screen has exited, then load the next screen in the series.
            if (ScreenState == ScreenState.Exited)
            {
                ScreenManager.AddScreen(new HighscoresDisplay(), null);
                ScreenManager.RemoveScreen(this);
            }

        }

        /// <summary>
        /// Draws the objects on the HSTakePicture
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            ScreenManager.GraphicsDevice.Clear(ClearOptions.Target,
                                               Color.Black, 0, 0);
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);

            spriteBatch.DrawString(scoreFont, "Congratulations! You made a new highscore of: " + ScoreManager.currentScore, new Vector2(275, 0), Color.Chartreuse);

                if (pictureCountdown < 10000)
                {
                    spriteBatch.DrawString(scoreFont, "Smile! Your picture will be taken in: " + (10 - pictureCountdown / 1000).ToString("N"), new Vector2(275, 75), Color.Chartreuse);
                    //Draw the video from Kinect camera
                    if (colorVideo != null) { spriteBatch.Draw(colorVideo, new Rectangle(320, 125, colorVideo.Width, colorVideo.Height), Color.White); }
                }
                else
                {
                    spriteBatch.DrawString(scoreFont, "Your picture was taken", new Vector2(275, 75), Color.Chartreuse);
                    if (pictureTaken == false && GameServices.GetService<KinectChooser>().Sensor != null) { takePicture(); pictureTaken = true; }
                }

            spriteBatch.End();
        }

        /// <summary>
        /// Video Frame Handler, modified tutorial from: http://kinectxna.blogspot.com/2012/02/tutorial-3-getting-video-stream.html
        /// By default Kinect ColorFrame outputs in BRGBA. Convert to BGRA 
        /// </summary>
        /// <param name="sender">Kinect object</param>
        /// <param name="colorImageFrame"></param>
        private void kinect_ColorFrameReady(object sender, ColorImageFrameReadyEventArgs colorImageFrame)
        {
            //Get raw image
            using (ColorImageFrame colorVideoFrame = colorImageFrame.OpenColorImageFrame())
            {

                if (colorVideoFrame != null)
                {
                    //Create array for pixel data and copy it from the image frame
                    Byte[] pixelData = new Byte[colorVideoFrame.PixelDataLength];
                    colorVideoFrame.CopyPixelDataTo(pixelData);

                    //Convert RGBA to BGRA
                    Byte[] bgraPixelData = new Byte[colorVideoFrame.PixelDataLength];
                    for (int i = 0; i < pixelData.Length; i += 4)
                    {
                        bgraPixelData[i] = pixelData[i + 2];
                        bgraPixelData[i + 1] = pixelData[i + 1];
                        bgraPixelData[i + 2] = pixelData[i];
                        bgraPixelData[i + 3] = (Byte)255; //The video comes with 0 alpha so it is transparent
                    }

                    // Create a texture and assign the realigned pixels
                    GraphicsDeviceManager graphics = GameServices.GetService<GraphicsDeviceManager>();
                    colorVideo = new Texture2D(graphics.GraphicsDevice, colorVideoFrame.Width, colorVideoFrame.Height);
                    colorVideo.SetData(bgraPixelData);
                }
            }
        }

        /// <summary>
        /// Takes picture from  Kinect stream and disables ColorStream
        /// </summary>
        private void takePicture()
        {
            //Take picture and save picture
            using (Stream streamOpenWrite = File.OpenWrite(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "TitaniumGoat", picturefilename)))
            {
                colorVideo.SaveAsPng(streamOpenWrite, 640, 480);
            }

            KinectChooser kinect = GameServices.GetService<KinectChooser>();
            kinect.Sensor.ColorStream.Disable();
        }
    }
}
