﻿//-----------------------------------------------------------------------------
// This code was created using an XNA sample code project as inspiration.
// However, it only somewhat resembles the original code.
// You can find the original sample code at this link;
// http://xbox.create.msdn.com/en-US/education/catalog/sample/game_state_management
// Licence = Microsoft Public Licence (MS-PL)
//-----------------------------------------------------------------------------

using System;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using TitaniumGoatKinectLib;

namespace TitaniumGoat
{
    /// <summary>
    /// Highscores Display (HSD) at the end of the game
    /// Also takes picture if played game is a high score
    /// </summary>
    public class HighscoresDisplay : GameScreen
    {
        
        private ContentManager content;
        private SpriteFont scoreFont;
        private Vector2 scoreFontPos;

        private static Texture2D highscorePicture;

        // Bootstrapping the old level data into this new format.
        protected Texture2D texBackground;
        

        // Button animations
        private Animation btnPlayAnimation;
        private Animation btnQuitAnimation;

        // Bounding boxes for screen buttons
        private Rectangle btnPlay = new Rectangle(150, 600, 300, 93);
        private Rectangle btnQuit = new Rectangle(775, 600, 300, 93);

        // Variables for mouse pointer positioning
        private Texture2D pointerTexture;
        private Vector2 pointerPosition = new Vector2();
        private SpriteFont pointerFont;

        private float timeElapsed;
        private string highscoresAsString;
        private Texture2D bananas;


        /// <summary>
        /// Constructor for HighscoresDisplay Screen
        /// </summary>
        public HighscoresDisplay()
        {
        }

        /// <summary>
        /// Load content for HSD
        /// </summary>
        public override void LoadContent()
        {
            if (content == null)
            {
                content = new ContentManager(ScreenManager.Game.Services, "Content");
            }

            texBackground = content.Load<Texture2D>(@"Backgrounds\\HighScoresBackground");

            // load texture for mouse cursor
            pointerTexture = content.Load<Texture2D>(@"Pointer");
            pointerFont = content.Load<SpriteFont>(@"Fonts\\DebugFont");

            // Load animations for buttons
            btnPlayAnimation = new Animation(content, @"Menus\\Buttons\\PieLoader", new Vector2(3, 3), 5, 0);
            btnPlayAnimation.MakePaused(true);
            btnPlayAnimation.Loop(false);
            btnQuitAnimation = new Animation(content, @"Menus\\Buttons\\PieLoader", new Vector2(3, 3), 5, 0);
            btnQuitAnimation.MakePaused(true);
            btnQuitAnimation.Loop(false);

            scoreFont = content.Load<SpriteFont>(@"Fonts\\ComicSansMS");
            scoreFontPos = new Vector2(650,300);

            //Do this so ScoreManager.getHighScoresAsString(), and the File IO operation in the method only gets loaded once, not every Draw loop.
            highscoresAsString = ScoreManager.getHighScoresAsString();

            bananas = content.Load<Texture2D>(@"HighScoreFiles\\bananaicon");

            //Load something as highscore picture incase there are no images for loadhighscorePicture()
            highscorePicture = content.Load<Texture2D>(@"Obstacles\\Dno1");
            loadhighscorePictures();

            // once the load has finished, we use ResetElapsedTime to tell the game's
            // timing mechanism that we have just finished a very long frame, and that
            // it should not try to catch up.
            ScreenManager.Game.ResetElapsedTime();
        }

        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void UnloadContent()
        {
            content.Unload();
        }


        /// <summary>
        /// Updates the state of the game. This method checks the GameScreen.IsActive
        /// property, so the game will stop updating when the pause menu is active,
        /// or if you tab away to a different application.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);

            //Increase the timeElapsed as gametime increases
            timeElapsed += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (IsActive)
            {
                // Call update for animations
                btnPlayAnimation.Update();
                btnQuitAnimation.Update();

                // Update mouse pointer position if no Kinect is found
                if (GameServices.GetService<KinectChooser>().Sensor == null || !KinectInputHandler.skeletonTracked)
                {
                    MouseState ms = Mouse.GetState();
                    pointerPosition.X = ms.X;
                    pointerPosition.Y = ms.Y;
                }
                else
                {
                    // Update mouse pointer position (kinect)
                    pointerPosition.X = KinectInputHandler.kinectMouseX;
                    pointerPosition.Y = KinectInputHandler.kinectMouseY;
                }

                // Do stuff if we have a click event
                // Are they clicking on the play button?
                if (btnPlay.Contains((int)pointerPosition.X, (int)pointerPosition.Y))
                {
                    btnPlayAnimation.MakePaused(false);
                    btnPlayAnimation.Reverse(false);

                    if (btnPlayAnimation.OnLastFrame())
                    {
                        DebugBuffer.write("Transitioning to level select menu...");
                        ScreenManager.AddScreen(new LevelSelectScreen(), null);
                    }
                }
                else
                { btnPlayAnimation.Reverse(true); }

                //Are they hovering over the Quit button?
                if (btnQuit.Contains((int)pointerPosition.X, (int)pointerPosition.Y))
                {
                    btnQuitAnimation.MakePaused(false);
                    btnQuitAnimation.Reverse(false);

                    if (btnQuitAnimation.OnLastFrame())
                    {
                        DebugBuffer.write("Transitioning to main menu...");
                        ScreenManager.AddScreen(new MainMenuScreen(), null);
                    }
                }
                else { btnQuitAnimation.Reverse(true); }

            }

            // If the screen has exited, then load the next screen in the series.
            if (ScreenState == ScreenState.Exited)
            {
                ScreenManager.AddScreen(new MainMenuScreen(), null);
                ScreenManager.RemoveScreen(this);
            }

        }


        /// <summary>
        /// Draws the objects on the HSD
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            ScreenManager.GraphicsDevice.Clear(ClearOptions.Target,
                                               Color.Black, 0, 0);
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);
            spriteBatch.Draw(texBackground, new Vector2(0, 0), null, Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 1);

            //Draw score related stuff
            spriteBatch.DrawString(scoreFont, highscoresAsString, scoreFontPos, Color.Crimson);
            spriteBatch.DrawString(scoreFont, "Your score was: " + ScoreManager.currentScore, new Vector2(450, 75), Color.Chartreuse);


            spriteBatch.Draw(bananas, new Rectangle(350, 90, bananas.Width, bananas.Height), Color.White);
            if (ScoreManager.getCollisionCount < 6) { spriteBatch.Draw(bananas, new Rectangle(350 + bananas.Width, 90, bananas.Width, bananas.Height), Color.White); }
            if (ScoreManager.getCollisionCount < 2) spriteBatch.Draw(bananas, new Rectangle(350 + 2 * (bananas.Width), 90, bananas.Width, bananas.Height), Color.White);

            spriteBatch.Draw(highscorePicture, new Rectangle(200, 275, 320, 240), Color.White);

            //Simple image gallery rotate of highscore pictures
            if (timeElapsed > 5000)
            {
                loadhighscorePictures();
                timeElapsed = 0;
            }

            //Draw Animation for buttons
            btnPlayAnimation.Draw(spriteBatch, new Vector2(200, 600));
            btnQuitAnimation.Draw(spriteBatch, new Vector2(800, 600));

            //Draw mouse on the screen if it is in the window
            spriteBatch.Draw(pointerTexture, pointerPosition, null, Color.White, 0f, Vector2.Zero, 1, SpriteEffects.None, 0.1f);

        #if DEBUG
            // Mouse cursor coordinates only get drawn if the game is running in
            // a debug mode
            spriteBatch.DrawString(pointerFont, pointerPosition.ToString(), pointerPosition, Color.White,
                                0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0f);
        #endif

            spriteBatch.End();
            
        }

        /// <summary>
        /// Loads a random picture taken from previous highscores
        /// </summary>
        private void loadhighscorePictures()
        {
            string[] highscorePictureArray = Directory.GetFiles(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "TitaniumGoat"), "*.png");

            if (highscorePictureArray.Length > 0)
            {
                Random random = new Random();
                using (FileStream streamOpenRead = File.OpenRead(highscorePictureArray[random.Next(highscorePictureArray.Length - 1)]))
                {
                    highscorePicture = Texture2D.FromStream(ScreenManager.GraphicsDevice, streamOpenRead);
                }
            }
        }
        
    }
}
