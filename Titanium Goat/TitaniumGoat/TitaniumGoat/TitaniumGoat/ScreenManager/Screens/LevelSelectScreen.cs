﻿//-----------------------------------------------------------------------------
// This code was created using an XNA sample code project as inspiration.
// However, it only somewhat resembles the original code.
// You can find the original sample code at this link;
// http://xbox.create.msdn.com/en-US/education/catalog/sample/game_state_management
// Licence = Microsoft Public Licence (MS-PL)
//-----------------------------------------------------------------------------

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using TitaniumGoatKinectLib;

namespace TitaniumGoat
{
    /// <summary>
    /// Presented to the player after the MainMenuScreenm, but before the
    /// main gameplay starts
    /// </summary>
    public class LevelSelectScreen : GameScreen
    {
        private ContentManager content;
        private float pauseAlpha;

        // Variables for mouse pointer positioning
        private Texture2D pointerTexture;
        private Vector2 pointerPosition = new Vector2();
        private SpriteFont pointerFont;

        // Button animations
        private Animation selectForest;
        private Animation selectDesert;
        private Animation selectVolcano;
        private Animation returntoMainMenuAnimation;

        // Bounding boxes for screen buttons
        private Rectangle btnReturntoMainMenu = new Rectangle(0, 521, 185, 185);
        private Rectangle btnForest = new Rectangle(0, 0, 612, 290);
        private Rectangle btnDesert = new Rectangle(225, 386, 450, 304);
        private Rectangle btnVolcano = new Rectangle(780, 320, 426, 380);

        private static int levelSelection;

        // Background texture drawn behind all other elements
        protected Texture2D texBackground;

        /// <summary>
        /// Constructor.
        /// </summary>
        public LevelSelectScreen()
        {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
        }

        /// <summary>
        /// Returns selected level
        /// </summary>
        public static int getlevelSelection
        {
            get { return levelSelection; }
        }

        /// <summary>
        /// Load graphics content for the game.
        /// </summary>
        public override void LoadContent()
        {
            if (content == null)
                content = new ContentManager(ScreenManager.Game.Services, "Content");

            texBackground = content.Load<Texture2D>(@"Menus\\Static\\LevelSelect");

            // load texture for mouse cursor
            pointerTexture = content.Load<Texture2D>(@"Pointer");
            pointerFont = content.Load<SpriteFont>(@"Fonts\\DebugFont");

            //Get selection animations ready to do there thing!
            selectForest = new Animation(content, @"Menus\\Buttons\\PieLoader", new Vector2(3, 3), 5, 0);
            selectForest.MakePaused(true);
            selectForest.Loop(false);
            selectDesert = new Animation(content, @"Menus\\Buttons\\PieLoader", new Vector2(3, 3), 5, 0);
            selectDesert.MakePaused(true);
            selectDesert.Loop(false);
            selectVolcano = new Animation(content, @"Menus\\Buttons\\PieLoader", new Vector2(3, 3), 5, 0);
            selectVolcano.MakePaused(true);
            selectVolcano.Loop(false);
            returntoMainMenuAnimation = new Animation(content, @"Menus\\Buttons\\PieLoader", new Vector2(3, 3), 5, 0);
            returntoMainMenuAnimation.MakePaused(true);
            returntoMainMenuAnimation.Loop(false);

            // once the load has finished, we use ResetElapsedTime to tell the game's
            // timing mechanism that we have just finished a very long frame, and that
            // it should not try to catch up.
            ScreenManager.Game.ResetElapsedTime();
        }


        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void UnloadContent()
        {
            content.Unload();
        }

        /// <summary>
        /// Updates the state of the game. This method checks the GameScreen.IsActive
        /// property, so the game will stop updating when the pause menu is active,
        /// or if you tab away to a different application.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);

            // Gradually fade in or out depending on whether we are covered by the pause screen.
            if (coveredByOtherScreen)
                pauseAlpha = Math.Min(pauseAlpha + 1f / 32, 1);
            else
                pauseAlpha = Math.Max(pauseAlpha - 1f / 32, 0);

            if (IsActive)
            {
                // Update selection animations
                selectForest.Update();
                selectDesert.Update();
                selectVolcano.Update();
                returntoMainMenuAnimation.Update();

                // Update mouse pointer position if no Kinect is found
                if (GameServices.GetService<KinectChooser>().Sensor == null || !KinectInputHandler.skeletonTracked)
                {
                    MouseState ms = Mouse.GetState();
                    pointerPosition.X = ms.X;
                    pointerPosition.Y = ms.Y;
                }
                else
                {
                    // Update mouse pointer position (kinect)
                    pointerPosition.X = KinectInputHandler.kinectMouseX;
                    pointerPosition.Y = KinectInputHandler.kinectMouseY;
                }

                // Make selections if the pointer is hovering over a selectable object
                //Select Forest level
                if (btnForest.Contains((int)pointerPosition.X, (int)pointerPosition.Y))
                {

                    selectForest.MakePaused(false);
                    selectForest.Reverse(false);

                    if (selectForest.OnLastFrame())
                    {
                        DebugBuffer.write("Transitioning to next menu...");
                        IsExiting = true;
                        levelSelection = 1;
                    }
                }
                else
                {
                    selectForest.Reverse(true);
                }

                //Select Desert level
                if (btnDesert.Contains((int)pointerPosition.X, (int)pointerPosition.Y))
                {
                    selectDesert.MakePaused(false);
                    selectDesert.Reverse(false);

                    if (selectDesert.OnLastFrame())
                    {
                        DebugBuffer.write("Transitioning to next menu...");
                        IsExiting = true;
                        levelSelection = 2;
                    }

                }
                else
                {
                    selectDesert.Reverse(true);
                }

                //Select Volcano level
                if (btnVolcano.Contains((int)pointerPosition.X, (int)pointerPosition.Y))
                {
                    selectVolcano.MakePaused(false);
                    selectVolcano.Reverse(false);

                    if (selectVolcano.OnLastFrame())
                    {
                        DebugBuffer.write("Transitioning to next menu...");
                        IsExiting = true;
                        levelSelection = 3;
                    }

                }
                else
                {
                    selectVolcano.Reverse(true);
                }

                //Select to return to main menu
                if (btnReturntoMainMenu.Contains((int)pointerPosition.X, (int)pointerPosition.Y))
                {
                    returntoMainMenuAnimation.MakePaused(false);
                    returntoMainMenuAnimation.Reverse(false);

                    if (returntoMainMenuAnimation.OnLastFrame())
                    {
                        DebugBuffer.write("Returing to main menu...");
                        IsExiting = true;
                        levelSelection = -1;
                    }

                }
                else
                {
                    returntoMainMenuAnimation.Reverse(true);
                }
            }

            // If the screen has exited, then load the next screen in the series.
            if (ScreenState == ScreenState.Exited)
            {
                if (levelSelection != -1)
                {
                    ScreenManager.AddScreen(new GameplayScreen(levelSelection), null);
                }
                else
                {
                    ScreenManager.AddScreen(new MainMenuScreen(), null);
                }
                ScreenManager.RemoveScreen(this);
                ScoreManager.reset();
            }

        }

        /// <summary>
        /// Draws the screen.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            ScreenManager.GraphicsDevice.Clear(ClearOptions.Target,
                                               Color.Black, 0, 0);
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);

            // Draw selection animations
            selectForest.Draw(spriteBatch, new Vector2(136, 65));
            selectDesert.Draw(spriteBatch, new Vector2(296, 412));
            selectVolcano.Draw(spriteBatch, new Vector2(851, 391));
            returntoMainMenuAnimation.Draw(spriteBatch, new Vector2(0, 500));

            //Draw mouse on the screen if it is in the window
            spriteBatch.Draw(pointerTexture, pointerPosition, null, Color.White, 0f, Vector2.Zero, 1, SpriteEffects.None, 0.1f);

            #if DEBUG
            // Mouse cursor coordinates only get drawn if the game is running in
            // a debug mode
            spriteBatch.DrawString(pointerFont, pointerPosition.ToString(), pointerPosition, Color.White,
                                0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0f);
            #endif

            spriteBatch.Draw(texBackground, new Vector2(0, 0), null, Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 1);
            spriteBatch.End();

            if (TransitionPosition > 0 || pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);
                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }
    }
}