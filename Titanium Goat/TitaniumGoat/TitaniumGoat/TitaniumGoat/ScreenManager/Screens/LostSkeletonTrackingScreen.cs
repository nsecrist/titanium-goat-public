﻿//-----------------------------------------------------------------------------
// This code was created using an XNA sample code project as inspiration.
// However, it only somewhat resembles the original code.
// You can find the original sample code at this link;
// http://xbox.create.msdn.com/en-US/education/catalog/sample/game_state_management
// Licence = Microsoft Public Licence (MS-PL)
//-----------------------------------------------------------------------------

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace TitaniumGoat
{
    /// <summary>
    /// The pause menu comes up over the top of the game,
    /// giving the player options to resume or quit.
    /// </summary>
    class LostSkeletonTrackingScreen : GameScreen
    {
        private ContentManager content;
        private int updateCount = 0;
        private float timeElapsed;

        private SpriteFont Font;
        private Vector2 WarningFontPos;
        private Vector2 FoundFontPos;

        /// <summary>
        /// Default constructor
        /// </summary>
        public LostSkeletonTrackingScreen()
        {
        }

        public override void LoadContent()
        {
            if (content == null)
            {
                content = new ContentManager(ScreenManager.Game.Services, "Content");
            }

            Font = content.Load<SpriteFont>(@"Fonts\\ComicSansMS");
            WarningFontPos = new Vector2(320, 200);
            FoundFontPos = new Vector2(400, 400);
            base.LoadContent();
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {

            //Count out the first second of the pause menu being active
            //this solves an issue I was having where the pause menu
            //would dissapear as soon as it was created because the escape
            //key was getting counted twice.
            if (updateCount < 60) updateCount++;

            if (IsActive && updateCount == 60)
            {
                KeyboardState currentKeyboardState = Keyboard.GetState();

                if (currentKeyboardState.IsKeyDown(Keys.Tab))
                {
                    DebugBuffer.write(gameTime.TotalGameTime + " Overriding lost skeleton screen...");
                    ScreenManager.RemoveScreen(this);
                }

                if (KinectInputHandler.skeletonTracked == true)
                    timeElapsed += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                if (timeElapsed > 5000)
                {
                    DebugBuffer.write(gameTime.TotalGameTime + "Skeleton found! Resuming...");
                    ScreenManager.RemoveScreen(this);
                }
            }
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            spriteBatch.Begin();
            spriteBatch.DrawString(Font, "Oh no! The Kinect can't see you. \n-Make sure you are standing infront of the Kinect \n-Make sure there aren't any obstacles", WarningFontPos, Color.Red);
            if (KinectInputHandler.skeletonTracked == true && timeElapsed < 5000)
            {
                spriteBatch.DrawString(Font, "There you go! We found you again.\nThe game will start in: " + (5 - (timeElapsed / 1000)).ToString("N") + "seconds", FoundFontPos, Color.PowderBlue);
            }

            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
