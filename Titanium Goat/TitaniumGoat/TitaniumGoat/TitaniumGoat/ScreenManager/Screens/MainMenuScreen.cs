﻿//-----------------------------------------------------------------------------
// This code was created using an XNA sample code project as inspiration.
// However, it only somewhat resembles the original code.
// You can find the original sample code at this link;
// http://xbox.create.msdn.com/en-US/education/catalog/sample/game_state_management
// Licence = Microsoft Public Licence (MS-PL)
//-----------------------------------------------------------------------------

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using TitaniumGoatKinectLib;

namespace TitaniumGoat
{
    /// <summary>
    /// Will contain controls for exiting the game or starting a new session
    /// This Screen gets created once the KinectWarningScreen finishes it's call.
    /// </summary>
    public class MainMenuScreen : GameScreen
    {
        private ContentManager content;
        private float pauseAlpha;

        // Button animations
        private Animation playAnimation;
        private Animation optionsAnimation;
        private Animation exitAnimation;

        // Bounding boxes for screen buttons
        private Rectangle btnPlay = new Rectangle(560, 240, 256, 192);
        private Rectangle btnOptions = new Rectangle(900, 475, 256, 192);
        private Rectangle btnExit = new Rectangle(50, 475, 155, 148);

        // Variables for mouse pointer positioning
        private Texture2D pointerTexture;
        private Vector2 pointerPosition = new Vector2();
        private SpriteFont pointerFont;

        // Background texture. Drawn behind all other screen elements
        protected Texture2D texBackground;

        /// <summary>
        /// Constructor.
        /// </summary>
        public MainMenuScreen()
        {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
        }

        /// <summary>
        /// Load graphics content for the game.
        /// </summary>
        public override void LoadContent()
        {
            if (content == null)
                content = new ContentManager(ScreenManager.Game.Services, "Content");

            texBackground = content.Load<Texture2D>(@"Menus\\Static\\MainRedesign");

            // load texture for mouse cursor
            pointerTexture = content.Load<Texture2D>(@"Pointer");
            pointerFont = content.Load<SpriteFont>(@"Fonts\\DebugFont");

            // Load animations for buttons
            playAnimation = new Animation(content, @"Menus\\Buttons\\PieLoader", new Vector2(3, 3), 5, 0);
            playAnimation.MakePaused(true);
            playAnimation.Loop(false);
            optionsAnimation = new Animation(content, @"Menus\\Buttons\\PieLoader", new Vector2(3, 3), 5, 0);
            optionsAnimation.MakePaused(true);
            optionsAnimation.Loop(false);
            exitAnimation = new Animation(content, @"Menus\\Buttons\\PieLoader", new Vector2(3, 3), 5, 0);
            exitAnimation.MakePaused(true);
            exitAnimation.Loop(false);

            // once the load has finished, we use ResetElapsedTime to tell the game's
            // timing mechanism that we have just finished a very long frame, and that
            // it should not try to catch up.
            ScreenManager.Game.ResetElapsedTime();
        }


        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void UnloadContent()
        {
            content.Unload();
        }

        /// <summary>
        /// Updates the state of the game. This method checks the GameScreen.IsActive
        /// property, so the game will stop updating when the pause menu is active,
        /// or if you tab away to a different application.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);

            // Gradually fade in or out depending on whether we are covered by the pause screen.
            if (coveredByOtherScreen)
                pauseAlpha = Math.Min(pauseAlpha + 1f / 32, 1);
            else
                pauseAlpha = Math.Max(pauseAlpha - 1f / 32, 0);

            if (IsActive)
            {
                // Call update for animations
                playAnimation.Update();
                optionsAnimation.Update();
                exitAnimation.Update();

                // Update mouse pointer position if no Kinect is found
                if (GameServices.GetService<KinectChooser>().Sensor == null || !KinectInputHandler.skeletonTracked)
                {
                    MouseState ms = Mouse.GetState();
                    pointerPosition.X = ms.X;
                    pointerPosition.Y = ms.Y;
                }
                else
                {
                    // Update mouse pointer position (kinect)
                    pointerPosition.X = KinectInputHandler.kinectMouseX;
                    pointerPosition.Y = KinectInputHandler.kinectMouseY;
                }

                // Do stuff if we have a click event
                // Are they clicking on the play button?
                if (btnPlay.Contains((int)pointerPosition.X, (int)pointerPosition.Y))
                {
                    playAnimation.MakePaused(false);
                    playAnimation.Reverse(false);

                    if (playAnimation.OnLastFrame())
                    {
                        DebugBuffer.write("Transitioning to level select menu...");
                        IsExiting = true;
                    }
                }
                else
                {
                    playAnimation.Reverse(true);
                }

                // Same test for options button
                if (btnOptions.Contains((int)pointerPosition.X, (int)pointerPosition.Y))
                {
                    optionsAnimation.MakePaused(false);
                    optionsAnimation.Reverse(false);

                    if (optionsAnimation.OnLastFrame())
                    {
                        DebugBuffer.write("Transitioning to option menu...");
                        ScreenManager.AddScreen(new OptionScreen(), null);
                    }

                }
                else
                {
                    optionsAnimation.Reverse(true);
                }

                //Same test for exit button
                if (btnExit.Contains((int)pointerPosition.X, (int)pointerPosition.Y))
                {
                    exitAnimation.MakePaused(false);
                    exitAnimation.Reverse(false);

                    if (exitAnimation.OnLastFrame())
                    {
                        DebugBuffer.write("EXITING!");

                        //Cleanup before exiting.
                        if (GameServices.GetService<KinectChooser>().Sensor != null)
                        {
                            GameServices.GetService<KinectChooser>().Sensor.Stop();
                        }
                        Environment.Exit(0); //Should probably ask if player wants to exit 
                    }
                }
                else
                {
                    exitAnimation.Reverse(true);
                }
            }

            // If the screen has exited, then load the next screen in the series.
            if (ScreenState == ScreenState.Exited)
            {
                ScreenManager.AddScreen(new LevelSelectScreen(), null);
                ScreenManager.RemoveScreen(this);
            }

        }



        /// <summary>
        /// Draws the screen.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            ScreenManager.GraphicsDevice.Clear(ClearOptions.Target,
                                               Color.Black, 0, 0);
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);

            //Draw buttons
            playAnimation.Draw(spriteBatch, new Vector2(520,200));
            optionsAnimation.Draw(spriteBatch, new Vector2(1000, 475));
            exitAnimation.Draw(spriteBatch, new Vector2(50, 475));

            //Draw mouse on the screen if it is in the window
            spriteBatch.Draw(pointerTexture, pointerPosition, null, Color.White, 0f, Vector2.Zero, 1, SpriteEffects.None, 0.1f);

#if DEBUG
            // Mouse cursor coordinates only get drawn if the game is running in
            // a debug mode
            spriteBatch.DrawString(pointerFont, pointerPosition.ToString(), pointerPosition, Color.White,
                                0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0f);
#endif

            //Draw the background
            spriteBatch.Draw(texBackground, new Vector2(0, 0), null, Color.White, 0f, Vector2.Zero, 1, SpriteEffects.None, 1f);
            
            spriteBatch.End();

            if (TransitionPosition > 0 || pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);
                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }
    }
}
