﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace TitaniumGoat
{
    /// <summary>
    /// Class for creating the option screen.
    /// </summary>
    class OptionScreen : GameScreen
    {

        string[] items = { "Low", "Medium", "High" }; //audiolevel
        string[] lightLevels = { "Light", "Medium", "Dark" };


        Texture2D left;
        Texture2D right;
        Texture2D slider;
        LinkButton back = new LinkButton();
        LeftRightSelection music = new LeftRightSelection();
        Slider light;

        ScreenObjectManager som;




        SpriteBatch spriteBatch;    
        SpriteFont debugFont;



        /// <summary>
        /// Constructor for option screen.
        /// </summary>
        public OptionScreen()
        {
        }



        /// <summary>
        /// Loads the content needed for the option screen.
        /// </summary>
        public override void LoadContent()
        {
            ContentManager content = new ContentManager(ScreenManager.Game.Services, "Content");//Game.Content;
            spriteBatch = ScreenManager.SpriteBatch;
            left = content.Load<Texture2D>(@"Menus\\Controllers\\leftarrow");
            right = content.Load<Texture2D>(@"Menus\\Controllers\\rightarrow");
            slider = content.Load<Texture2D>(@"Menus\\Controllers\\slider");
            debugFont = content.Load<SpriteFont>(@"Fonts\\DebugFont");
            som = new ScreenObjectManager(debugFont);

            music = new LeftRightSelection(left, right, slider);
            music.LoadContent(content, null);
            music.setList(items, 125);
            music.setPosition(new Vector2(400,300));

            som.Add(music);

            light = new Slider(slider);
            light.setPosition(new Vector2(400,400));
            light.setText("light", 100);
            light.LoadContent(content, null);

            som.Add(light);


            back.setPosition(new Vector2(800,500));
            back.Selected += new EventHandler(linkLabel1_Selected);
            back.LoadContent(content, @"Menus/Controllers/backarrow");

            som.Add(back);

 

        }

        /// <summary>
        /// Event handler for when an event happens for the back button.
        /// </summary>
        void linkLabel1_Selected(object sender, EventArgs e)
        {
            DebugBuffer.write(" Exiting option menu...");
            ScreenManager.RemoveScreen(this);
        }


        /// <summary>
        /// Updates the components associated with the option menu.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {

            som.Update(gameTime, PlayerIndex.One);

            if (KeyboardInputHandler.KeyPressed(Keys.Escape))
            {
                DebugBuffer.write(" Exiting option menu...");
                ScreenManager.RemoveScreen(this);
            }


        }

        /// <summary>
        /// Draws the components associated with the option menu.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();

            som.Draw(spriteBatch);



            spriteBatch.End();
        }


 
    }
}
