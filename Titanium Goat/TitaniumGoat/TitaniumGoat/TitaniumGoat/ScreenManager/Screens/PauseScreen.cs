﻿//-----------------------------------------------------------------------------
// This code was created using an XNA sample code project as inspiration.
// However, it only somewhat resembles the original code.
// You can find the original sample code at this link;
// http://xbox.create.msdn.com/en-US/education/catalog/sample/game_state_management
// Licence = Microsoft Public Licence (MS-PL)
//-----------------------------------------------------------------------------

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace TitaniumGoat
{
    /// <summary>
    /// The pause menu comes up over the top of the game,
    /// giving the player options to resume or quit.
    /// </summary>
    class PauseScreen : GameScreen
    {
        private int updateCount = 0;

        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {

            //Count out the first second of the pause menu being active
            //this solves an issue I was having where the pause menu
            //would dissapear as soon as it was created because the escape
            //key was getting counted twice.
            if (updateCount < 60) updateCount++;


            if (IsActive && updateCount == 60)
            {
                KeyboardState currentKeyboardState = Keyboard.GetState();
                if (currentKeyboardState.IsKeyDown(Keys.Tab))
                {
                    DebugBuffer.write(gameTime.TotalGameTime + " Disabling pause menu...");
                    ScreenManager.RemoveScreen(this);
                }
            }
        }
    }


}
