﻿//-----------------------------------------------------------------------------
// This code was created using an XNA sample code project as inspiration.
// However, it only somewhat resembles the original code.
// You can find the original sample code at this link;
// http://xbox.create.msdn.com/en-US/education/catalog/sample/game_state_management
// Licence = Microsoft Public Licence (MS-PL)
//-----------------------------------------------------------------------------

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace TitaniumGoat
{
    /// <summary>
    /// This screen displays a brief message about the games use of the
    /// Kinect sensor. Once it fades away, it loads up the Main menu,
    /// where the player can select where they want to go.
    /// </summary>
    public class TitleScreen : GameScreen
    {
        private ContentManager content;
        private float pauseAlpha;

        // Bootstrapping the old level data into this new format.
        protected Texture2D texBackground;

        // This is used as a delay counter for this warning screen.
        // The screen will automatically exit when the counter reaches
        // zero
        private int timeOutCountdown = 180;

        /// <summary>
        /// Constructor.
        /// </summary>
        public TitleScreen()
        {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(5);
        }

        /// <summary>
        /// Load graphics content for the game.
        /// </summary>
        public override void LoadContent()
        {
            if (content == null)
                content = new ContentManager(ScreenManager.Game.Services, "Content");

            texBackground = content.Load<Texture2D>(@"Menus\\Static\\TitleScreen");

            // once the load has finished, we use ResetElapsedTime to tell the game's
            // timing mechanism that we have just finished a very long frame, and that
            // it should not try to catch up.
            ScreenManager.Game.ResetElapsedTime();
        }

        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void UnloadContent()
        {
            content.Unload();
        }

        /// <summary>
        /// Updates the state of the game. This method checks the GameScreen.IsActive
        /// property, so the game will stop updating when the pause menu is active,
        /// or if you tab away to a different application.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);

            // Gradually fade in or out depending on whether we are covered by the pause screen.
            if (coveredByOtherScreen)
                pauseAlpha = Math.Min(pauseAlpha + 1f / 32, 1);
            else
                pauseAlpha = Math.Max(pauseAlpha - 1f / 32, 0);

            if (IsActive)
            {
                // Close the screen after the timeOut conter
                // reaches zero
                if (timeOutCountdown == 0)
                {
                    DebugBuffer.write("Automatic transition triggered by KinectWarningScreen.timeOutCountdown");
                    IsExiting = true;
                }
                else
                {
                    // Decrement the counter by 1 every frame
                    timeOutCountdown--;
                }

                //Check to see if user wants to pause
                KeyboardState currentKeyboardState = Keyboard.GetState();
                if (currentKeyboardState.IsKeyDown(Keys.Escape) && pauseAlpha == 0)
                {
                    DebugBuffer.write("Automatic transition triggered by KinectWarningScreen.timeOutCountdown");
                    IsExiting = true;
                }
            }

            // If the screen has exited, then load the next screen in the series.
            if (ScreenState == ScreenState.Exited)
            {
                ScreenManager.AddScreen(new KinectWarningScreen(), null);
                ScreenManager.RemoveScreen(this);
            }

        }

        /// <summary>
        /// Draws the warning screen.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            ScreenManager.GraphicsDevice.Clear(ClearOptions.Target,
                                               Color.Black, 0, 0);
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);
            spriteBatch.Draw(texBackground, new Vector2(0, 0), null, Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 1);
            spriteBatch.End();

            if (TransitionPosition > 0 || pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);
                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }
    }
}
