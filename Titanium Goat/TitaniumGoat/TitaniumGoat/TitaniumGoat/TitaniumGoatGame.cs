using Microsoft.Kinect;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.IO;
using TitaniumGoatKinectLib;
namespace TitaniumGoat
{

    /// <summary>
    /// The main execution class of the game
    /// </summary>
    public class TitaniumGoatGame : Microsoft.Xna.Framework.Game
    {

        /// <summary>
        /// This is the SpriteBatch used for rendering the header/footer.
        /// </summary>
        private SpriteBatch spriteBatch;

        // The graphics object manages painting on the screen
        private GraphicsDeviceManager graphics;
        private int lastStateChange = 0; // Prevents graphics device from changing state too quickly

        // The content object manages the loading of images, sounds and fonts from disk
        private ContentManager content;

        private ScreenManager screenManager;
        private DebugBufferRenderer debugConsole;

        // Audio mixer files
        protected Song songImDaBes;
        protected Song songHappyWalk;

        private readonly KinectChooser kinectChooser;

        public TitaniumGoatGame()
        {
            //Create a save directory for TitaniumGoat under C:\Users\<Username>\AppData\Roaming\TitaniumGoat\
            string saveDirectoryPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "TitaniumGoat");
            if (!Directory.Exists(saveDirectoryPath))
            {
                try
                {
                    Directory.CreateDirectory(saveDirectoryPath);
                    DebugBuffer.write("Save path was created at: " + saveDirectoryPath);
                }

                catch (Exception e)
                {
                    DebugBuffer.write("WARNING: Creating the save directory failed {0}" + e.ToString());
                }
            }
            else
            {
                DebugBuffer.write("Save path exists at: " + saveDirectoryPath);
            }

            // Set up the base content manager
            Content.RootDirectory = "Content";
            content = new ContentManager(Services);

            // Get the screen ready to start painting graphics
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;
            
            // Create the screen manager component that manages transitions
            // and drawing of screens
            screenManager = new ScreenManager(this);
            Components.Add(screenManager);
            screenManager.AddScreen(new TitleScreen(), null);

            Components.Add(new KeyboardInputHandler(this));
            
            // Doing the same thing here that we did for the level
            // Adding the debugConsole to the Components list makes XNA
            // call it's update and draw methods on every frame.
            debugConsole = new DebugBufferRenderer(this);
            Components.Add(debugConsole);

            // Get the Kinect sensor identified and added as XNA GameService so it is accessible statically. 
            this.kinectChooser = new KinectChooser(this, ColorImageFormat.RgbResolution640x480Fps30, DepthImageFormat.Resolution640x480Fps30);
            this.Services.AddService(typeof(KinectChooser), this.kinectChooser);
            GameServices.AddService<KinectChooser>(this.kinectChooser);

            this.Services.AddService(typeof(GraphicsDeviceManager), this.graphics);
            GameServices.AddService<GraphicsDeviceManager>(this.graphics);


            DebugBuffer.write("Current Kinect status = " + kinectChooser.getStatus());
            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here


            // Register all gestures used in the application
            KinectInputHandler kinectInput = new KinectInputHandler();
            kinectInput.RegisterGestures();

            //Load save game file
            ScoreManager.Load();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Load local audio samples
            songImDaBes = Content.Load<Song>(@"Audio\\BGM\\IMDABES_Instrumental");
            songHappyWalk = Content.Load<Song>(@"Audio\\BGM\\Happy_walk");

            this.spriteBatch = new SpriteBatch(this.GraphicsDevice);
            this.Services.AddService(typeof(SpriteBatch), this.spriteBatch);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
            if (kinectChooser.Sensor != null)
            {
                kinectChooser.Sensor.Stop();
            }
            content.Unload();
        }

        protected override void Update(GameTime gameTime)
        {

            // Start playing after KinectWarningScreen
#if !NO_AUDIO
            Type typeOfScreen = screenManager.CurrentScreenType();
            if(typeOfScreen == typeof(MainMenuScreen))
            {
                //if (MediaPlayer.Queue.ActiveSong != songImDaBes) MediaPlayer.Play(songImDaBes);
                //MediaPlayer.Stop();
            }
            if(typeOfScreen == typeof(GameplayScreen))
            {
                if (MediaPlayer.Queue.ActiveSong != songHappyWalk) MediaPlayer.Play(songHappyWalk); MediaPlayer.Volume = 0.1f;
            }
            if(typeOfScreen == typeof(HighscoresDisplay))
            {
                if (MediaPlayer.Queue.ActiveSong != songImDaBes) MediaPlayer.Play(songImDaBes);
            }
            if(typeOfScreen == typeof(MainMenuScreen))
            {
                if (MediaPlayer.Queue.ActiveSong != songImDaBes) MediaPlayer.Play(songImDaBes);
            }
            if ((typeOfScreen == typeof(PauseScreen) || typeOfScreen == typeof(LostSkeletonTrackingScreen)) && !(MediaPlayer.State == MediaState.Paused))
            {
                MediaPlayer.Pause();
            }

#endif

            // Check to see if the F11 key is pressed. If it is, toggle the game window to or
            // from full screen mode.
            KeyboardState currentKeyboardState = Keyboard.GetState();
            if (currentKeyboardState.IsKeyDown(Keys.F11) && lastStateChange == 0)
            {
                graphics.ToggleFullScreen();
                lastStateChange = 30;
                DebugBuffer.write("Window state is now set to: FULLSCREEN = " + graphics.IsFullScreen);
            }
            else
            {
                if (lastStateChange > 0) lastStateChange--;
            }

            if (IsActive)
            {
                if (MediaPlayer.State == MediaState.Paused)
                {
                    MediaPlayer.Resume();
                }
            }
            else
            {
                MediaPlayer.Pause();
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// Draws things on the screen
        /// </summary>
        /// <param name="gameTime">Game runtime timings</param>
        protected override void Draw(GameTime gameTime)
        {
            // Clear the screen so we can draw a new frame
            graphics.GraphicsDevice.Clear(Color.Black);
            base.Draw(gameTime);
        }
    }
}
