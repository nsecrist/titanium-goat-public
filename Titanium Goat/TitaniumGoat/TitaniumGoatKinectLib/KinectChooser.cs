﻿// This code was adapted from a sample project in the Microsoft Kinect Developer Toolkit v1.6
// The code was made available under MS-PL "Microsoft Public License"
// The developer toolkit can be downloaded from; http://www.microsoft.com/en-us/download/details.aspx?id=36998
// The sample project name was; "XNA Basics"

using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Kinect;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TitaniumGoatKinectLib
{

    /// <summary>
    /// This class will pick a kinect sensor if available.
    /// </summary>
    public class KinectChooser : DrawableGameComponent
    {
        /// <summary>
        /// The status to string mapping.
        /// </summary>
        private readonly Dictionary<KinectStatus, string> statusMap = new Dictionary<KinectStatus, string>();

        /// <summary>
        /// The requested color image format.
        /// </summary>
        private readonly ColorImageFormat colorImageFormat;

        /// <summary>
        /// The requested depth image format.
        /// </summary>
        private readonly DepthImageFormat depthImageFormat;

        /// <summary>
        /// Initializes a new instance of the KinectChooser class.
        /// </summary>
        /// <param name="game">The related game object.</param>
        /// <param name="colorFormat">The desired color image format.</param>
        /// <param name="depthFormat">The desired depth image format.</param>
        public KinectChooser(Game game, ColorImageFormat colorFormat, DepthImageFormat depthFormat)
            : base(game)
        {
            this.colorImageFormat = colorFormat;
            this.depthImageFormat = depthFormat;

            KinectSensor.KinectSensors.StatusChanged += this.KinectSensors_StatusChanged;
            this.DiscoverSensor();

            this.statusMap.Add(KinectStatus.Connected, string.Empty);
            this.statusMap.Add(KinectStatus.DeviceNotGenuine, "Device Not Genuine");
            this.statusMap.Add(KinectStatus.DeviceNotSupported, "Device Not Supported");
            this.statusMap.Add(KinectStatus.Disconnected, "Required");
            this.statusMap.Add(KinectStatus.Error, "Error");
            this.statusMap.Add(KinectStatus.Initializing, "Initializing...");
            this.statusMap.Add(KinectStatus.InsufficientBandwidth, "Insufficient Bandwidth");
            this.statusMap.Add(KinectStatus.NotPowered, "Not Powered");
            this.statusMap.Add(KinectStatus.NotReady, "Not Ready");
        }

        /// <summary>
        /// Gets the selected KinectSensor.
        /// </summary>
        public KinectSensor Sensor { get; private set; }

        /// <summary>
        /// Gets the last known status of the KinectSensor.
        /// </summary>
        public KinectStatus LastStatus { get; private set; }

        public string getStatus()
        {
            string txt = "UNDEFINED";
            if (this.Sensor != null)
            {
                txt = this.statusMap[this.LastStatus];
                switch (this.LastStatus)
                {
                    case KinectStatus.Connected:
                        txt = "Connected";
                        break;
                    case KinectStatus.DeviceNotGenuine:
                        txt = "Connected device is not genuine";
                        break;
                    case KinectStatus.DeviceNotSupported:
                        txt = "The connected device is not supported";
                        break;
                    case KinectStatus.Disconnected:
                        txt = "The device has been disconnected";
                        break;
                    case KinectStatus.Error:
                        txt = "An unknown error has occured";
                        break;
                    case KinectStatus.Initializing:
                        txt = "Initializing...";
                        break;
                    case KinectStatus.InsufficientBandwidth:
                        txt = "Insufficient bandwidth on USB bus";
                        break;
                    case KinectStatus.NotPowered:
                        txt = "Device is not powered, is the A/C adapter connected?";
                        break;
                    case KinectStatus.NotReady:
                        txt = "Not ready";
                        break;
                    case KinectStatus.Undefined:
                        txt = "Device defenition is not defined";
                        break;
                    default:
                        txt = "Device is in an unknown state";
                        break;
                }
            }
            else
            {
                txt = "DISCONNECTED";
            }
            return txt;
        }

        /// <summary>
        /// This method initializes necessary objects.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }
        
        /// <summary>
        /// This method loads the textures and fonts.
        /// </summary>
        protected override void LoadContent()
        {
            base.LoadContent();
        }

        /// <summary>
        /// This method ensures that the KinectSensor is stopped before exiting.
        /// </summary>
        protected override void UnloadContent()
        {
            base.UnloadContent();

            // Always stop the sensor when closing down
            if (this.Sensor != null)
            {
                this.Sensor.Stop();
            }
        }

        /// <summary>
        /// This method will use basic logic to try to grab a sensor.
        /// Once a sensor is found, it will start the sensor with the
        /// requested options.
        /// </summary>
        private void DiscoverSensor()
        {
            // Grab any available sensor
            this.Sensor = KinectSensor.KinectSensors.FirstOrDefault();

            if (this.Sensor != null)
            {
                this.LastStatus = this.Sensor.Status;

                // If this sensor is connected, then enable it
                if (this.LastStatus == KinectStatus.Connected)
                {
                    TransformSmoothParameters parameters = new TransformSmoothParameters();
                    parameters.Smoothing = 0.4f;
                    parameters.Correction = 0.3f;
                    parameters.Prediction = 0.4f;
                    parameters.JitterRadius = 1.0f;
                    parameters.MaxDeviationRadius = 0.5f;

                    this.Sensor.SkeletonStream.Enable(parameters);
                    this.Sensor.ColorStream.Enable(this.colorImageFormat);
                    this.Sensor.DepthStream.Enable(this.depthImageFormat);

                    try
                    {
                        this.Sensor.Start();
                    }
                    catch (IOException)
                    {
                        // sensor is in use by another application
                        // will treat as disconnected for display purposes
                        this.Sensor = null;
                    }
                }
            }
            else
            {
                this.LastStatus = KinectStatus.Disconnected;
            }
        }

        /// <summary>
        /// This wires up the status changed event to monitor for 
        /// Kinect state changes.  It automatically stops the sensor
        /// if the device is no longer available.
        /// </summary>
        /// <param name="sender">The sending object.</param>
        /// <param name="e">The event args.</param>
        private void KinectSensors_StatusChanged(object sender, StatusChangedEventArgs e)
        {
            // If the status is not connected, try to stop it
            if (e.Status != KinectStatus.Connected)
            {
                e.Sensor.Stop();
            }

            this.LastStatus = e.Status;
            this.DiscoverSensor();
        }
    }
}
