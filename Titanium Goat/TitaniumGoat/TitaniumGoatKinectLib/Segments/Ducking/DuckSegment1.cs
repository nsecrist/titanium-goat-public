﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Kinect;

namespace TitaniumGoatKinectLib.Segments
{
    public class DuckSegment1 : IRelativeGestureSegment
    {
        /// <summary>
        /// Checks the gesture.
        /// </summary>
        /// <param name="skeleton">The skeleton.</param>
        /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
        public GesturePartResult CheckGesture(Skeleton skeleton)
        {
            // knees are in front of feet
            if (skeleton.Joints[JointType.KneeRight].Position.Z < skeleton.Joints[JointType.FootRight].Position.Z &&
                skeleton.Joints[JointType.KneeLeft].Position.Z < skeleton.Joints[JointType.FootLeft].Position.Z)
            {
                //head are in front of knees
                if (skeleton.Joints[JointType.Head].Position.Z < skeleton.Joints[JointType.KneeRight].Position.Z ||
                    skeleton.Joints[JointType.Head].Position.Z < skeleton.Joints[JointType.KneeLeft].Position.Z)
                {
                    return GesturePartResult.Succeed;
                }

                return GesturePartResult.Pausing;
            }

            return GesturePartResult.Fail;
        }
    }
}
