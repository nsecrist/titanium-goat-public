﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Kinect;

namespace TitaniumGoatKinectLib.Segments
{
    public class SelectBottomLeftSegment : IRelativeGestureSegment
    {
        public GesturePartResult CheckGesture(Skeleton skeleton)
        {
            if (skeleton.Joints[JointType.KneeLeft].Position.Y > skeleton.Joints[JointType.KneeRight].Position.Y)
            {
                if (skeleton.Joints[JointType.FootLeft].Position.Y > skeleton.Joints[JointType.AnkleRight].Position.Y)
                {
                    if (skeleton.Joints[JointType.FootLeft].Position.Z < skeleton.Joints[JointType.FootRight].Position.Z)
                    {
                        return GesturePartResult.Succeed;
                    }
                    return GesturePartResult.Pausing;
                }
                return GesturePartResult.Pausing;
            }
            return GesturePartResult.Fail;
        }
    }
}
