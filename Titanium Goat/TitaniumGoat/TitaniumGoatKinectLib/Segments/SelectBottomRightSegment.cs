﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Kinect;

namespace TitaniumGoatKinectLib.Segments
{
    public class SelectBottomRightSegment : IRelativeGestureSegment
    {

        public GesturePartResult CheckGesture(Skeleton skeleton)
        {
            if (skeleton.Joints[JointType.KneeRight].Position.Y > skeleton.Joints[JointType.KneeLeft].Position.Y)
            {
                if (skeleton.Joints[JointType.FootRight].Position.Y > skeleton.Joints[JointType.AnkleLeft].Position.Y)
                {
                    if (skeleton.Joints[JointType.FootRight].Position.Z < skeleton.Joints[JointType.FootLeft].Position.Z)
                    {
                        return GesturePartResult.Succeed;
                    }
                    return GesturePartResult.Pausing;
                }
                return GesturePartResult.Pausing;
            }
            return GesturePartResult.Fail;
        }

    }
}
