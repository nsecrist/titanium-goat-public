﻿using TitaniumGoat;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace TitaniumGoatTests
{
    /// <summary>
    /// Summary description for ControllerTests
    /// </summary>
    [TestClass]
    public class ControllerTests
    {
        public ControllerTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void LinkButtonTest()
        {
            LinkButton link = new LinkButton();

            Assert.AreEqual(link.getPosition(), Vector2.Zero);
            link.setPosition(new Vector2(100, 100));
            Assert.AreEqual(link.getPosition(), new Vector2(100,100));
            Assert.IsFalse(link.isFocus);
            Assert.IsTrue(link.enabled);
            link.setSize(new Vector2(200, 200));
            Assert.AreEqual(link.getSize(), new Vector2(200, 200));
            link.handleInput(PlayerIndex.One);
            Assert.IsFalse(link.isFocus);
            Assert.IsTrue(link.enabled);
            // No input so no value should change
            //Assert.IsFalse(InputHandler.KeyDown(Keys.Up));
            //Assert.IsFalse(InputHandler.KeyReleased(Keys.Up));
            //
            // TODO: Add test logic here
            //
        }

        [TestMethod]
        public void LeftRightTest()
        {
            string[] items = {"l","m","h"};
            LeftRightSelection lr = new LeftRightSelection();
            Assert.AreEqual(lr.getPosition(), Vector2.Zero);
            lr.setList(items, 100);
            Assert.AreEqual(lr.getList(),items);
            Assert.AreEqual(lr.getSelectedItem(), "l");
            lr.handleInput(PlayerIndex.One);
            // No input so no value should change
            Assert.AreEqual(lr.getList(), items);
            Assert.AreEqual(lr.getSelectedItem(), "l");
            //Assert.IsFalse(InputHandler.KeyDown(Keys.Up));
           // Assert.IsFalse(InputHandler.KeyReleased(Keys.Up));
            //
            // TODO: Add test logic here
            //
        }

        [TestMethod]
        public void SliderTest()
        {
            Slider s = new Slider();
            Assert.AreEqual(s.getPosition(), Vector2.Zero);
            Assert.AreEqual(s.getCurValue(), 0);
            s.handleInput(PlayerIndex.One); 
            // No input so no value should change
            Assert.AreEqual(s.getCurValue(), 0);
            //Assert.IsFalse(InputHandler.KeyDown(Keys.Up));
            //Assert.IsFalse(InputHandler.KeyReleased(Keys.Up));
            //
            // TODO: Add test logic here
            //
        }
    }
}
