﻿using TitaniumGoat;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TitaniumGoatTests
{    
    /// <summary>
    ///This is a test class for DebugBufferTest and is intended
    ///to contain all DebugBufferTest Unit Tests
    ///</summary>
    [TestClass()]
    public class DebugBufferTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for clear
        ///</summary>
        [TestMethod()]
        public void DebugBufferClearTest()
        {
            DebugBuffer.clear();
            Assert.IsTrue(DebugBuffer.toString().Length == 0);
        }

        /// <summary>
        ///A test for toString
        ///</summary>
        [TestMethod()]
        public void DebugBufferToStringTest()
        {
            //Write five lines to the buffer
            DebugBuffer.write("I'm a little tea pot,");
            DebugBuffer.write("Short and stout,");
            DebugBuffer.write("This is my handle, this is my spout,");
            DebugBuffer.write("When I get all steamed up, hear me shout!");
            DebugBuffer.write("So, tip me over and pour me out!");
            DebugBuffer.write("YUM!");
            DebugBuffer.write("Coding is fun!");

            //When we call ToString, we should get the last four lines that were posted
            string expected = "Short and stout," + '\n'
                                + "This is my handle, this is my spout," + '\n'
                                + "When I get all steamed up, hear me shout!" + '\n'
                                + "So, tip me over and pour me out!" + '\n'
                                + "YUM!" + '\n'
                                + "Coding is fun!" + '\n';
            string actual;
            actual = DebugBuffer.toString();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for write
        ///</summary>
        [TestMethod()]
        public void DebugBufferWriteTest()
        {
            //Reset testing object
            DebugBuffer.clear();
            string input = "Hello, World!";
            string expected = "Hello, World!" + '\n';

            DebugBuffer.write(input);

            string actual = DebugBuffer.toString();
            
            Assert.AreEqual(actual, expected);
        }
    }
}
