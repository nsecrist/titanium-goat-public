﻿using TitaniumGoat;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace TitaniumGoatTests
{


    /// <summary>
    ///This is a test class for EnemyTest and is intended
    ///to contain all EnemyTest Unit Tests
    ///</summary>
    [TestClass]
    public class EnemyTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion



        /// <summary>
        ///A test for Enemy Constructor
        ///</summary>
        [TestMethod()]
        public void EnemyConstructorTest1()
        {
            Enemy target = new laneEnemy();
            Assert.AreEqual(target.getPosition(), Vector2.Zero);
            Assert.AreEqual(target.getVelocity(), Vector2.Zero);
            Assert.AreEqual(target.getSize(), new Vector2(100, 100));
            target = new laneEnemy(0, 1000, null, 0);
            Assert.AreEqual(target.getPosition(), new Vector2(571, 370));
            target = new laneEnemy(1, 1000, null, 0);
            Assert.AreEqual(target.getPosition(), new Vector2(700, 370));
            target = new laneEnemy(2, 1000, null, 0);
            Assert.AreEqual(target.getPosition(), new Vector2(845, 370));
            target = new largeEnemy(1000, null, 0);
            Assert.AreEqual(target.getPosition(), new Vector2(700, 370));
        }


        /// <summary>
        /// Update test
        ///</summary>
        [TestMethod()]
        public void updateTest()
        {
            Enemy target = new laneEnemy(0, 1000, null, 0);
            
            target.Update(null);
            Assert.AreEqual(target.getCount, 1);
            Assert.AreEqual(target.getPosition(), new Vector2(568.9f, 374.8f));

            target = new laneEnemy(1, 1000, null, 0);
            target.Update(null);
            Assert.AreEqual(target.getPosition(), new Vector2(700, 374.8f));

            target = new laneEnemy(2, 1000, null, 0);
            target.Update(null);

            Assert.AreEqual(target.getPosition(), new Vector2(847.1f, 374.8f));
            target = new largeEnemy(1000, null, 0);
            target.Update(null);
            Assert.AreEqual(target.getPosition(), new Vector2(700, 374.8f));

        }

        /// <summary>
        ///A test for getCount
        ///</summary>
        [TestMethod()]
        public void getCountTest()
        {
            Enemy target = new laneEnemy();
            int expected = 0;
            int actual;
            actual = target.getCount;
            Assert.AreEqual(expected, actual);

        }

        /// <summary>
        ///A test for getLane
        ///</summary>
        [TestMethod()]
        public void getLaneTest()
        {
            Enemy target = new laneEnemy();
            int expected = 0;
            int actual;
            actual = target.getLane;
            Assert.AreEqual(expected, actual);
        }


        /// <summary>
        ///A test for getScale
        ///</summary>
        [TestMethod()]
        public void getScaleTest()
        {
            Enemy target = new laneEnemy();
            float expected = 1F;
            float actual;
            actual = target.getScale;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for getState
        ///</summary>
        [TestMethod()]
        public void getStateTest()
        {
            Enemy target = new laneEnemy();
            int expected = 0;
            int actual;
            actual = target.getState;
            Assert.AreEqual(expected, actual);
        }


        /// <summary>
        ///A test for Active
        ///</summary>
        [TestMethod()]
        public void ActiveTest()
        {
            Enemy target = new laneEnemy();
            bool expected = false;
            bool actual;
            target.Active = expected;
            actual = target.Active;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for PassedPlayer
        ///</summary>
        [TestMethod()]
        public void PassedPlayerTest()
        {
            Enemy target = new laneEnemy();
            bool expected = true;
            Assert.AreEqual(false, target.PassedPlayer);
            bool actual;
            target.PassedPlayer = expected;
            actual = target.PassedPlayer;
            Assert.AreEqual(expected, actual);
        }
    }
}
