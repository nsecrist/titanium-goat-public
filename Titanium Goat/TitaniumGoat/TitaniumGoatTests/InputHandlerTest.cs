﻿using TitaniumGoat;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace TitaniumGoatTests
{


    /// <summary>
    ///This is a test class for MathProblemTest and is intended
    ///to contain all MathProblemTest Unit Tests
    ///</summary>
    [TestClass()]
    public class InputHandlerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for MathProblem Constructor
        ///</summary>
        ///




        [TestMethod]
        public void KeysTest()
        {
            Game game = new Game();
            game.Components.Add(new KeyboardInputHandler(game));
            Assert.IsFalse(KeyboardInputHandler.KeyPressed(Keys.Up));
            Assert.IsFalse(KeyboardInputHandler.KeyDown(Keys.Up));
            Assert.IsFalse(KeyboardInputHandler.KeyReleased(Keys.Up));

            KeyboardState keyboardState = KeyboardInputHandler.KeyboardState;
            KeyboardState lastKeyboardState = KeyboardInputHandler.LastKeyboardState;
            Assert.AreEqual(keyboardState, lastKeyboardState);

            
            keyboardState = KeyboardInputHandler.KeyboardState;
            lastKeyboardState = KeyboardInputHandler.LastKeyboardState;
            Assert.AreEqual(keyboardState, lastKeyboardState);

            game.Dispose();
        }

    }
}
