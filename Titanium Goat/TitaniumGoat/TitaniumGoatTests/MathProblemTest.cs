﻿using TitaniumGoat;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TitaniumGoatTests
{


    /// <summary>
    ///This is a test class for MathProblemTest and is intended
    ///to contain all MathProblemTest Unit Tests
    ///</summary>
    [TestClass()]
    public class MathProblemTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance; 
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for MathProblem Constructor
        ///</summary>
        [TestMethod()]
        public void MathProblemConstructorTest()
        {
            bool passed = true;

            // Testing randomly generated problems
            for (int i = 0; i < 1000; i++)
            {
                MathProblem target = new MathProblem();

                if (target.getOperator == '/')
                {
                    if (target.getLeftOperand < target.getRightOperand) passed = false;
                    else if ((target.getLeftOperand % target.getRightOperand) != 0) passed = false;
                    else if (target.getRightOperand == 0) passed = false;
                }
                else if (target.getOperator == '-')
                {
                    if (target.getLeftOperand < target.getRightOperand) passed = false;
                }

            }

            // Testing addition problems
            for (int i = 0; i < 250; i++)
            {
                MathProblem target = new MathProblem(ProblemType.addition, Hidden.equals);

                if (target.getOperator != '+') { passed = false; };
            }

            // Testing subtraction problems
            for (int i = 0; i < 250; i++)
            {
                MathProblem target = new MathProblem(ProblemType.subtraction, Hidden.equals);

                if (target.getOperator != '-') { passed = false; };
            }

            // Testing multiplication problems
            for (int i = 0; i < 250; i++)
            {
                MathProblem target = new MathProblem(ProblemType.multiplication, Hidden.equals);

                if (target.getOperator != '*') { passed = false; };
            }

            // Testing division problems
            for (int i = 0; i < 250; i++)
            {
                MathProblem target = new MathProblem(ProblemType.division, Hidden.equals);

                if (target.getOperator != '/') { passed = false; };
            }

            Assert.IsTrue(passed);
        }

    }
}
