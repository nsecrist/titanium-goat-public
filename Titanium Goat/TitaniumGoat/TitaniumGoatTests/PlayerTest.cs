﻿using TitaniumGoat;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace TitaniumGoatTests
{
    /// <summary>
    /// Summary description for Character
    /// </summary>
    [TestClass]
    public class PlayerTest
    {
        public PlayerTest()
        {

            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void CreationTest()
        {
            Player mCharacter = new Player();
            Assert.AreEqual(mCharacter.getPosition(), new Vector2(600, 656));
            Assert.AreEqual(mCharacter.getVelocity(), new Vector2(0, 0));
            Assert.AreEqual(mCharacter.getSize(), new Vector2(200, 1000));
            Assert.AreEqual(mCharacter.getLane(), 1);
        }

        [TestMethod]
        public void stateTest()
        {
            Player mCharacter = new Player();
            Assert.AreEqual(0, mCharacter.getState());
            mCharacter.setState(1);
            Assert.AreEqual(1, mCharacter.getState());
        }
    }
}
