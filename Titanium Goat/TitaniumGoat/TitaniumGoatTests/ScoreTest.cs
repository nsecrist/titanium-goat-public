﻿// Assert methods: http://msdn.microsoft.com/en-us/library/ms245297.aspx

using TitaniumGoat;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO.IsolatedStorage;
using System.IO;

namespace TitaniumGoatTests
{


    /// <summary>
    ///This is a test class for ScoreTest and is intended
    ///to contain all ScoreTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ScoreTest
    {
        private static readonly string filename = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "TitaniumGoat", "highscores.xml");


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //

        //Use TestInitialize to run code before running each test
        [TestInitialize()]
        public void MyTestInitialize()
        {

            //Initialize some values to be saved to the file for LoadHighScoresFileTest()
            ScoreManager.Load();

            //ScoreManager.SaveHighScoresFile(data, filename);
        }


        //Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup()
        {
            File.Delete(filename);
            ScoreManager.reset();
        }

        #endregion

        /// <summary>
        ///A poorly written, overly undefined, test for get/set methods.
        ///</summary>
        [TestMethod()]
        public void GetAndSetTests()
        {
            //Initialize
            ScoreManager.increaseScore(1000);

            //Check that get functions are returning the right types
            Assert.IsInstanceOfType(ScoreManager.currentScore, typeof(int));
            //Check that increase functions are woring
            int initialScore = ScoreManager.currentScore;
            ScoreManager.increaseScore(500);
            int expectedScore = ScoreManager.currentScore - 500;
            Assert.AreEqual(initialScore, expectedScore);

            double initialMultiplier = 1.0;
            ScoreManager.increaseMultiplier(0.5);
            double expectedMultiplier = 1.5;
            ScoreManager.increaseScore(500);
            Assert.AreEqual(2250, ScoreManager.currentScore);
            Assert.AreEqual(expectedMultiplier, initialMultiplier + 0.5);
        }

        /// <summary>
        ///Test for Score being saved.
        ///</summary>
        [TestMethod()]
        public void SaveHighScore()
        {
            ScoreManager.increaseScore(9000);
            ScoreManager.Save();
            Assert.IsTrue(ScoreManager.isNewHighScore);

            ScoreManager.HighScoreData data = ScoreManager.Load();
            Assert.AreEqual(9000, data.Score[0]);
        }

        /// <summary>
        ///Test for ScoreManager.Save call when score isn't a highscore
        ///</summary>
        [TestMethod()]
        public void SavenotHighScore()
        {
            ScoreManager.increaseScore(10);
            ScoreManager.Save();

            Assert.IsFalse(ScoreManager.isNewHighScore);
        }

        //http://msdn.microsoft.com/en-us/library/ms243427.aspx
        /// <summary>
        ///Test for getHighScoresAsString method. Very genric for now until the method format is formally defined
        ///</summary>
        [TestMethod()]
        public void GetHighScoresAsStringTest()
        {
            string actual = ScoreManager.getHighScoresAsString();
            Assert.IsInstanceOfType(actual, typeof(string));

        }

    }
}
