﻿using TitaniumGoat;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TitaniumGoatTests
{
    /// <summary>
    ///This is a test class for ScreenManager
    ///</summary>
    [TestClass()]
    class ScreenManagerTests : GameScreen
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //

        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}


        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}

        #endregion

        [TestMethod()]
        public void ScreenManagerBlackboxTests()
        {
            Assert.IsInstanceOfType(ScreenManager.SpriteBatch, typeof(SpriteBatch));
            Assert.IsInstanceOfType(ScreenManager.Font, typeof(SpriteFont));
            Assert.IsInstanceOfType(ScreenManager.GetScreens(), typeof(GameScreen[]));
            //Assert.IsInstanceOfType(ScreenManager.CurrentScreenType, typeof(Type));
        }

        [TestMethod()]
        public void ScreenManagerAddRemoveScreenTests()
        {
            int initialLength = ScreenManager.GetScreens().Length;
            ScreenManager.AddScreen(new HighscoresDisplay(), null);
            int afterLength = ScreenManager.GetScreens().Length;
            Assert.AreEqual(initialLength, afterLength - 1);

        }


    }
}
